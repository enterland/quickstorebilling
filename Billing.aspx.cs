﻿using EntityFramework.Extensions;
using Microsoft.Reporting.WebForms;
using QuickStoreBilling.Model;
using QuickStoreBilling.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class Billing : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        static string billNumber;
        public int CategoryIDView = 0;
        public string TypeIDView = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "bl")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }

            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
            if (!IsPostBack)
            {
                getProductGrid(CategoryIDView, TypeIDView);
                getQuotationProducts();
                getCalculation();
                txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                fillDdlCustomers();
                fillDdlProducteCategotyView();
                fillDdlProducteTypeView();
                getCountQuotationItem();
            }
        }

        public void fillDdlProducteCategotyView()
        {
            ddlProductCategoryView.Items.Clear();
            var cat = db.ProductCategories.Select(m => new { id = m.CatId, name = m.name }).ToList();


            ddlProductCategoryView.DataSource = cat;
            ddlProductCategoryView.DataTextField = "name";
            ddlProductCategoryView.DataValueField = "id";
            ddlProductCategoryView.DataBind();
            ddlProductCategoryView.Items.Insert(0, "Select");
        }
        public void fillDdlProducteTypeView()
        {
            ddlProductTypeView.Items.Clear();
            var cat = db.ProductTypes.Select(m => new { name = m.name }).Distinct().ToList();

            ddlProductTypeView.DataSource = cat;
            ddlProductTypeView.DataTextField = "name";
            ddlProductTypeView.DataValueField = "name";
            ddlProductTypeView.DataBind();
            ddlProductTypeView.Items.Insert(0, "Select");
        }

        public void getCalculation()
        {
            decimal total = 0;
            decimal totalExclGst = 0;
            decimal GstTotal = 0;
            var s = db.Quotations;
            foreach (Quotation q in s)
            {
                totalExclGst += Convert.ToDecimal(q.unitPrice * q.Qty);
                total += Convert.ToDecimal(q.totalPrice);
                GstTotal += Convert.ToDecimal((q.unitPriceAll * q.Qty) - (q.unitPrice * q.Qty));
            }



            txtTotalExclGst.Text = string.Format("{0:#,##0.00}", totalExclGst);
            txtTotal.Text = total.ToString();
            txtGST.Text = string.Format("{0:#,##0.00}", GstTotal);

            decimal discount = (total * Decimal.Parse(txtDiscount_.Text) / 100);
            txtDiscount.Text = string.Format("{0:#,##0.00}", discount);
            txtNetTotal.Text = string.Format("{0:#,##0.00}", (total - Decimal.Parse(txtDiscount.Text)));

        }

        private void getProductGrid(int catId, string typeId)
        {
            if (catId > 0)
            {
                var s = db.Products.Where(l => l.CatId == catId && l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    ProductName = m.name,
                    Qty = m.csUnitQty * m.csQty + m.unitQty,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100)
                    + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    CategoryName = m.ProductCategory.name,
                    BoxQty = (int)m.csQty,
                    LoseQty = m.unitQty,
                    mrp = m.mrp
                    //BoxQty = m.csQty,
                    //InBoxNoOfQty = m.csUnitQty,
                    //SingleQty = m.unitQty
                })
               .OrderBy(k => k.CategoryName).ToList();

                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
            else if (typeId != "")
            {
                var s = db.Products.Where(l => l.ProductType.name == typeId && l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    ProductName = m.name,
                    Qty = m.csUnitQty * m.csQty + m.unitQty,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100)
                    + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    CategoryName = m.ProductCategory.name,
                    BoxQty = (int)m.csQty,
                    LoseQty = m.unitQty,
                    mrp = m.mrp
                    //BoxQty = m.csQty,
                    //InBoxNoOfQty = m.csUnitQty,
                    //SingleQty = m.unitQty
                })
               .OrderBy(k => k.CategoryName).ToList();

                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
            else
            {
                var s = db.Products.Where(l => l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    ProductName = m.name,
                    Qty = m.csUnitQty * m.csQty + m.unitQty,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100)
                    + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    CategoryName = m.ProductCategory.name,
                    BoxQty = (int)m.csQty,
                    LoseQty = m.unitQty,
                    mrp = m.mrp
                    // + "." + (int)m.unitQty//+"."+(int)m.unitQty
                    //BoxQty = m.csQty,
                    //InBoxNoOfQty = m.csUnitQty,
                    //SingleQty = m.unitQty
                })
               .OrderBy(k => k.CategoryName).ToList();

                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
        }

        public void fillDdlCustomers()
        {
            var cat = db.users.Where(s => s.userCatId == 2).Select(m => new { id = m.uId, name = m.name }).ToList();


            ddlCustomers.DataSource = cat;
            ddlCustomers.DataTextField = "name";
            ddlCustomers.DataValueField = "id";
            ddlCustomers.DataBind();
            ddlCustomers.Items.Insert(0, "Select");

            ddlInvoiceCustomer.DataSource = cat;
            ddlInvoiceCustomer.DataTextField = "name";
            ddlInvoiceCustomer.DataValueField = "id";
            ddlInvoiceCustomer.DataBind();
            ddlInvoiceCustomer.Items.Insert(0, "Select");
        }

        protected void txtQty_TextChanged(object sender, EventArgs e)
        {
            //TabName.Value = "tab1";
            TextBox txtQty = (TextBox)sender;
            GridViewRow row = (GridViewRow)txtQty.Parent.Parent;
            Label labTotalUnitQty = (Label)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("Label2");
            TextBox txtEnterUnitQty = (TextBox)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("txtQty");
            TextBox txtEnterCSQty = (TextBox)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("txtEntryByCS");

            if (labTotalUnitQty.Text == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Please update the stock!!!')</SCRIPT>");
                txtEnterUnitQty.Text = null;
                txtEnterCSQty.Text = null;
                return;
            }
            if (stockAvailableCheck(Decimal.Parse(labTotalUnitQty.Text), Decimal.Parse(txtEnterUnitQty.Text)))
            {
                Label labTotal = (Label)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("labTotal");
                Label labunitPrice = (Label)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("labUnitPrice");
                LinkButton lbAddToBill = (LinkButton)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("lbAddToBill");
                if (labunitPrice.Text != "")
                {
                    labTotal.Text = (Decimal.Parse(labunitPrice.Text) * Decimal.Parse(txtEnterUnitQty.Text)).ToString();
                    lbAddToBill.Visible = true;
                }

            }
            else
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + txtEnterUnitQty.Text + " Qty is not available into stocks. Please enter valid Qty.!!!')</SCRIPT>");
                txtEnterUnitQty.Text = null;
            }


        }

        private bool stockAvailableCheck(decimal stockQty, decimal enterQty)  // Check stock item Unit qty total is available or not
        {
            if (enterQty <= stockQty)
                return true;
            else
                return false;

        }

        protected void lbAddToBill_Click(object sender, EventArgs e)
        {
            //TabName.Value = "tab1";


            LinkButton lbAddToBill = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lbAddToBill.NamingContainer;
            int rowID = gvRow.RowIndex;

            int id = Convert.ToInt32(gvProducts.DataKeys[rowID].Values[0]);
            TextBox qty = (TextBox)gvProducts.Rows[gvRow.RowIndex].Cells[3].FindControl("txtQty");
            Label unitPrice = (Label)gvProducts.Rows[gvRow.RowIndex].Cells[3].FindControl("labUnitPrice");
            Label total = (Label)gvProducts.Rows[gvRow.RowIndex].Cells[3].FindControl("labTotal");


            Quotation quotationCheck = db.Quotations.SingleOrDefault(g => g.prodId == id);

            if (quotationCheck == null) // Check product is already added into quation or not
            {
                Quotation quoat = new Quotation();
                quoat.prodId = id;
                var selectedProd = db.Products.SingleOrDefault(m => m.ProdId == id);
                quoat.prodName = selectedProd.name;
                quoat.Qty = Decimal.Parse(qty.Text);
                quoat.unitPrice = Math.Round((decimal)((selectedProd.price / selectedProd.csUnitQty) + (selectedProd.price / selectedProd.csUnitQty) * selectedProd.sellingMargin / 100), 2);
                quoat.gst = Decimal.Parse(selectedProd.gst.ToString());
                quoat.unitPriceAll = Decimal.Parse(unitPrice.Text);
                quoat.totalPrice = Decimal.Parse(total.Text);
                quoat.quotationDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                quoat.uId = null;
                quoat.quotationId = null;

                db.Quotations.Add(quoat);
                db.SaveChanges();
                getQuotationProducts();
                getCountQuotationItem();
            }
            else
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert(' Product is already added...')</SCRIPT>");
        }

        public void getQuotationProducts()
        {
            var ld = db.Quotations;

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("id", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ProductName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Qty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("gst", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("totalQty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("unitPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("UnitPriceAll", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("TotalPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("BoxQty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("InBoxNoOfQty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SingleQty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("cs_SingleQty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("mrp", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("BoxDotSingleQty", Type.GetType("System.String")));

            foreach (Quotation m in ld)
            {
                DataRow dr = dt.NewRow();


                dr[0] = m.quotationLineId;
                dr[1] = m.prodName;
                dr[2] = m.Qty;
                dr[3] = m.gst;
                var x = db.Products.FirstOrDefault(l => l.ProdId == m.prodId);
                int qty = (int)(x.csQty * (x.csUnitQty) + (x.unitQty));
                dr[4] = qty;
                dr[5] = m.unitPrice;
                dr[6] = m.unitPriceAll;
                dr[7] = m.totalPrice;
                dr[8] = x.csQty;
                dr[9] = x.csUnitQty;
                dr[10] = x.unitQty;
                dr[11] = getCSnSingleQtyCalculation(m);  // the traditionl gridview bind with linq not supported any function call into data headerder.
                dr[12]= x.mrp;
                dr[13] = "" + (int)x.csQty + "." + (int)x.unitQty;


                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);

            gvOrder.DataSource = dd;
            gvOrder.DataBind();
            getCalculation();
        }

        private string getCSnSingleQtyCalculation(Quotation m)
        {

            var currentProd = db.Products.SingleOrDefault(l => l.ProdId == m.prodId);

            decimal a = (decimal)(m.Qty / currentProd.csUnitQty);
            decimal b = (a - Math.Floor(a));
            decimal c = (decimal)(currentProd.csUnitQty * b);
            c = Math.Round(c, 2);

            int i = (int)a;
            int f = (int)c;

            string x = i + "." + f;

            return x;
            //product.csQty = (int)CsQtyAdjust;
            //product.unitQty = (int)unitQtyAdjust;
        }

        protected void txtQtyOrder_TextChanged(object sender, EventArgs e)
        {
            //TabName.Value = "tab1";
            TextBox txtQtyOrder = (TextBox)sender;
            GridViewRow row = (GridViewRow)txtQtyOrder.Parent.Parent;
            int id = Convert.ToInt32(gvOrder.DataKeys[row.RowIndex].Values[0]);
            Label labTotalOredr = (Label)gvOrder.Rows[row.RowIndex].Cells[3].FindControl("labTotalOrder");
            Label labunitPriceOredr = (Label)gvOrder.Rows[row.RowIndex].Cells[3].FindControl("labUnitPriceOrder");
            Label labTotalUnitQty = (Label)gvOrder.Rows[row.RowIndex].Cells[3].FindControl("labTotalQtyOrder");
            TextBox txtEnterUnitQty = (TextBox)gvOrder.Rows[row.RowIndex].Cells[3].FindControl("txtQtyOrder");

            if (stockAvailableCheck(Decimal.Parse(labTotalUnitQty.Text), Decimal.Parse(txtEnterUnitQty.Text)))
            {
                if (labunitPriceOredr.Text != "")
                    labTotalOredr.Text = (Decimal.Parse(labunitPriceOredr.Text) * Decimal.Parse(txtEnterUnitQty.Text)).ToString();
                updateQuotation(id, Decimal.Parse(txtEnterUnitQty.Text), Decimal.Parse(labTotalOredr.Text));
                getCalculation();
                getCountQuotationItem();
            }
            else
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + txtEnterUnitQty.Text + " Qty is not available into stocks. Please enter valid Qty.!!!')</SCRIPT>");

        }



        protected void txtDiscount__TextChanged(object sender, EventArgs e)
        {
            getCalculation();
        }

        public void updateQuotation(int qid, decimal qty, decimal total)
        {
            Quotation quoat = db.Quotations.SingleOrDefault(l => l.quotationLineId == qid);

            if (qty == 0)
            {
                db.Quotations.Where(s => s.quotationLineId == qid).Delete();
                getQuotationProducts();
                return;
            }
            quoat.Qty = qty;
            quoat.totalPrice = total;

            db.SaveChanges();
        }


        public void userAutoComplete()
        {

        }

        protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCustomers.SelectedIndex != 0)
            {
                int ddlUid = Int32.Parse(ddlCustomers.SelectedItem.Value);
                var customer = db.users.SingleOrDefault(s => s.uId == ddlUid);

                txtCustomerName.Text = customer.name;
                txtContact.Text = customer.phone;
                txtGSTIN.Text = customer.GSTIN;
                var customerAddress = db.addresses.SingleOrDefault(k => k.uId == customer.uId);
                txtAddress.Text = customerAddress.adrLine1 + ", " + customerAddress.adrLine2 + ", " + customerAddress.districtCity + ", " + customerAddress.state + ", " + customerAddress.pin;

                ChkBxNewCuromer.Checked = false;
            }
            else
            {
                resetCustomer();
            }

        }
        private bool ValidNewUser()
        {
            //Do the duplicate check
            if (txtCustomerName.Text != "" && txtContact.Text != "")
                return true;
            else
                return false;
        }
        // Method for alfaNumaric OrderBy
        public static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }
        protected void btnQuoteToBillSubmit_Click(object sender, EventArgs e)
        {
            int OrderLineUid = 0;
            string OrderID = "";
            decimal profitMargin = 0;
            DateTime txt = Convert.ToDateTime(txtDate.Text);
            Order order = new Order();

            if (!ChkBxNewCuromer.Checked)
            {
                OrderLineUid = Int32.Parse(ddlCustomers.SelectedItem.Value);

                order.uid = OrderLineUid;
                order.addrId = db.addresses.Where(l => l.uId == OrderLineUid).FirstOrDefault().adrId;
            }
            else
            {
                if (!ValidNewUser())
                {
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Please check the customer details!!!')</SCRIPT>");
                    return;
                }

                user user = new user();
                user.name = txtCustomerName.Text;
                user.phone = txtContact.Text;
                user.GSTIN = txtGSTIN.Text;
                user.userCatId = 2;
                address addr = new address();
                addr.FullName = txtCustomerName.Text;
                addr.MobileNo = txtContact.Text;
                addr.adrLine1 = txtAddress.Text;
                addr.uId = user.uId;
                order.addrId = addr.adrId;


                OrderLineUid = user.uId;
                order.uid = user.uId;

                db.users.Add(user);
                db.addresses.Add(addr);

            }

            var oa = db.Orders.Select(m => new { orderId = m.orderId }).ToList();

            if (oa.Count == 0)
            {
                OrderID = "FRND/OD/" + (Convert.ToDateTime(txtDate.Text).Month) + "/" + (Convert.ToDateTime(txtDate.Text).Year) + "/" + 1;
            }
            else
            {
                var result = oa.OrderBy(k => PadNumbers(k.orderId)).LastOrDefault();// Orderby alfaNumeric bill number

                string[] LastOrderNo = result.orderId.Split('/');


                OrderID = "FRND/OD/" + (Convert.ToDateTime(txtDate.Text).Month) + "/" + (Convert.ToDateTime(txtDate.Text).Year)
                    + "/" + (Int32.Parse(LastOrderNo[4]) + 1);
            }


            order.orderId = OrderID;
            order.billDate = Convert.ToDateTime(txtDate.Text);
            order.billDiscount = Decimal.Parse(txtDiscount.Text);
            order.billTotal = Decimal.Parse(txtNetTotal.Text);

            order.gstAmount = Decimal.Parse(txtGST.Text);


            var quote = db.Quotations;

            foreach (Quotation q in quote)
            {
                OrderLine orderLine = new OrderLine();
                orderLine.uId = OrderLineUid;
                orderLine.prodId = q.prodId;
                orderLine.prodName = q.prodName;
                orderLine.Qty = q.Qty;
                orderLine.unitPrice = q.unitPrice;
                orderLine.gst = q.gst;
                orderLine.unitPriceAll = q.unitPriceAll;
                orderLine.totalPrice = q.totalPrice;
                orderLine.orderDate = q.quotationDate;
                orderLine.orderId = OrderID;

                Product product = db.Products.SingleOrDefault(s => s.ProdId == q.prodId);

                // Stock adjustment ///////////

                decimal totalProductQty = 0;
                if (product.unitQty != null)
                {
                    totalProductQty = (decimal)((product.csUnitQty * product.csQty) + product.unitQty);
                }
                else
                {
                    totalProductQty = (decimal)(product.csUnitQty * product.csQty);
                }



                decimal remProductQty = (decimal)(totalProductQty - q.Qty);

                decimal CsQtyAdjust = (decimal)(remProductQty / product.csUnitQty);

                decimal unitQtyAdjust = (decimal)(product.csUnitQty * (CsQtyAdjust - Math.Floor(CsQtyAdjust)));

                product.csQty = (int)CsQtyAdjust;
                product.unitQty = (int)unitQtyAdjust;

                //profit Calculation ///////////

                decimal currentUnitPriceAll = (decimal)(product.price / product.csUnitQty);
                currentUnitPriceAll = Math.Round(currentUnitPriceAll, 2);
                profitMargin += (decimal)(currentUnitPriceAll * q.Qty); //

                db.OrderLines.Add(orderLine);
            }

            order.profitMargin = (decimal)(Convert.ToDecimal(txtNetTotal.Text) - profitMargin); //

            db.Orders.Add(order);

            db.SaveChanges();


            // Invoice Tab changed
            fillDdlCustomers();
            fillDdlInvoiceBillNo((int)order.uid);
            ddlInvoiceCustomer.SelectedItem.Value = order.uid.ToString();
            ddlInvoiceBillNo.SelectedItem.Value = OrderID;

            TabName.Value = "tab2";

            DeleteAllQuotation();
            getQuotationProducts();

            getInvoiceReport((int)order.uid, OrderID);
        }

        private void DeleteAllQuotation()
        {
            db.Quotations.Delete();

            db.SaveChanges();
            labCountBillingItem.Text = "";
        }

        protected void ChkBxNewCuromer_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkBxNewCuromer.Checked)
            {
                ddlCustomers.SelectedIndex = 0;
                resetCustomer();

            }
        }

        public void resetCustomer()
        {
            txtCustomerName.Text = "";
            txtContact.Text = "";
            txtGSTIN.Text = "";
            txtAddress.Text = "";
        }

        protected void btnInvoice_Click(object sender, EventArgs e)
        {
            //Response.Redirect("/QuotationReport.aspx");
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "openModal", "window.open('/QuotationReport.aspx' ,'_blank');", true);
        }


        //======================//Invoice Report//==========================


        public void getInvoiceReport(int userId, string orderId)
        {
            try
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/OrderInvoice.rdlc");

                InvoiceDSClasss tr = new InvoiceDSClasss();
                DataSet ds = new DataSet();

                //bool islv = bool.Parse(Request.QueryString["is"]);
                ds = tr.getInvoice(userId, orderId);

                ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("InvoiceDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }


        private void fillDdlInvoiceBillNo(int uid)
        {

            var bil = db.Orders.Where(s => s.uid == uid).Select(m => new { id = m.orderId }).ToList();


            ddlInvoiceBillNo.DataSource = bil;
            ddlInvoiceBillNo.DataTextField = "id";
            ddlInvoiceBillNo.DataValueField = "id";
            ddlInvoiceBillNo.DataBind();
            ddlInvoiceBillNo.Items.Insert(0, "Select");
        }
        protected void ddlInvoiceCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            int userId = Int32.Parse(ddlInvoiceCustomer.SelectedItem.Value);
            fillDdlInvoiceBillNo(userId);

        }


        protected void ddlInvoiceBillNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            getInvoiceReport(Int32.Parse(ddlInvoiceCustomer.SelectedItem.Value), ddlInvoiceBillNo.SelectedItem.Value);
        }

        protected void ddlProductCategoryView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewCategory();
            getProductGrid(CategoryIDView, TypeIDView);
        }

        private void getCountQuotationItem()
        {
            int quotationItemCount = 0;

            quotationItemCount = db.Quotations.Count();
            if (quotationItemCount != 0)
                labCountBillingItem.Visible = true;
            labCountBillingItem.Text = "(" + quotationItemCount + ")";
        }
        /// <summary>
        /// End of invoice report
        /// </summary>
        private void selectedViewCategory() // gridview editing selcted viewcategory  
        {
            if (ddlProductCategoryView.SelectedIndex == 0)
                CategoryIDView = 0;
            else
                CategoryIDView = Int32.Parse(ddlProductCategoryView.SelectedItem.Value);

            ddlProductTypeView.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        private void filterGridSync() // for editiing, deleting and cancel selected filter syncronisiatin with grid
        {
            if (ddlProductTypeView.SelectedIndex > 0)
            {
                selectedViewType();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductCategoryView.SelectedIndex > 0)
            {
                selectedViewCategory();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductTypeView.SelectedIndex == 0 && ddlProductCategoryView.SelectedIndex == 0 && txtSearch.Text == "")
            {
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else
            {
                this.SearchText();
            }
        }
        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TabName.Value = "tab0";
            gvProducts.PageIndex = e.NewPageIndex;
            filterGridSync();
        }

        private void selectedViewType() // gridview editing selcted viewType  
        {
            if (ddlProductTypeView.SelectedIndex == 0)
                TypeIDView = "";
            else
                TypeIDView = ddlProductTypeView.SelectedItem.Text;

            ddlProductCategoryView.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        protected void ddlProductTypeView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewType();
            getProductGrid(CategoryIDView, TypeIDView);
        }
        /// dynamic serach////
        /// 

        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable GetRecords()

        {
            var s = db.Products.Where(l => l.IsDeactive != true).Select(m => new
            {
                id = m.ProdId,
                ProductName = m.name,
                Qty = m.csUnitQty * m.csQty + m.unitQty,
                UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100)
                + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                CategoryName = m.ProductCategory.name,
                BoxQty = (int)m.csQty,
                LoseQty = m.unitQty,
                mrp = m.mrp
                //BoxQty = m.csQty,
                //InBoxNoOfQty = m.csUnitQty,
                //SingleQty = m.unitQty
            })
               .OrderBy(k => k.CategoryName).ToList();

            gvProducts.DataSource = s;
            gvProducts.DataBind();

            DataTable dt = LINQResultToDataTable(s);

            return dt;
        }
        private void BindGrid()
        {
            DataTable dt = GetRecords();
            if (dt.Rows.Count > 0)
            {
                gvProducts.DataSource = dt;
                gvProducts.DataBind();
            }
        }

        private void SearchText()
        {
            DataTable dt = GetRecords();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvProducts.SortExpression, txtSearch.Text);
            }

            if (SearchExpression != null)
            {
                dv.RowFilter = "ProductName like" + SearchExpression;
                gvProducts.DataSource = dv;
                gvProducts.DataBind();
            }
        }

        public string Highlight(string InputTxt)
        {
            string Search_Str = txtSearch.Text.ToString();
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(),
            RegexOptions.IgnoreCase);

            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt,
            new MatchEvaluator(ReplaceKeyWords));

            // Set the RegExp to null.
            RegExp = null;
        }

        public string ReplaceKeyWords(Match m)
        {
            return "<span class='highlight'>" + m.Value + "</span>";

        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchText();
            ddlProductCategoryView.SelectedIndex = -1;
        }

        protected void txtEntryByCS_TextChanged(object sender, EventArgs e)
        {
            TextBox txtEntryByCS = (TextBox)sender;
            GridViewRow row = (GridViewRow)txtEntryByCS.Parent.Parent;
            TextBox txtEnterCSQty = (TextBox)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("txtEntryByCS");
            TextBox txtEnterUnitQty = (TextBox)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("txtQty");

            //valid cs qty and single item qty by decimal fraction part such as 10.2 === 10 is the cs qty and .2 is the single item qty

            int id = Convert.ToInt32(gvProducts.DataKeys[row.RowIndex].Values[0]);


            if (txtEnterCSQty.Text.Substring(0, 1) == ".")
                txtEnterCSQty.Text = "0" + txtEnterCSQty.Text;

            if (!txtEnterCSQty.Text.Contains('.'))
                txtEnterCSQty.Text += ".0";
            string[] cs_SingleQty = txtEnterCSQty.Text.Split('.');

            var selectedProduct = db.Products.SingleOrDefault(s => s.ProdId == id);
            if (selectedProduct.csQty < Int32.Parse(cs_SingleQty[0])
                || (selectedProduct.unitQty + selectedProduct.csUnitQty) < Int32.Parse(cs_SingleQty[1]))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>" +
                    "alert('" + txtEnterCSQty.Text + " Qty is not available into stocks. Please enter valid Qty.!!!')</SCRIPT>");
                txtEnterCSQty.Text = null;
                txtEnterUnitQty.Text = null;
                return;
            }

            int totalSingleEntryQty = ((int)selectedProduct.csUnitQty * (int)Int32.Parse(cs_SingleQty[0])) + (int)Int32.Parse(cs_SingleQty[1]);

            txtEnterUnitQty.Text = totalSingleEntryQty.ToString();
            txtQty_TextChanged(sender, e);
            LinkButton lbAddToBill = (LinkButton)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("lbAddToBill");
            object sender1 = lbAddToBill;
            lbAddToBill_Click(sender1, e);
        }

        protected void txtEntryByCSOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtQtyOrderCS = (TextBox)sender;
            GridViewRow row = (GridViewRow)txtQtyOrderCS.Parent.Parent;
            int id = Convert.ToInt32(gvOrder.DataKeys[row.RowIndex].Values[0]);// quotaion line id

            TextBox txtQtyOrder = (TextBox)gvOrder.Rows[row.RowIndex].Cells[3].FindControl("txtQtyOrder");

            //selected product id

            var QuotationLineProductId = db.Quotations.SingleOrDefault(l => l.quotationLineId == id).prodId;

            //valid cs qty and single item qty by decimal fraction part such as 10.2 === 10 is the cs qty and .2 is the sing item qty

            if (txtQtyOrderCS.Text.Substring(0, 1) == ".")
                txtQtyOrderCS.Text = "0" + txtQtyOrderCS.Text;

            if (!txtQtyOrderCS.Text.Contains('.'))
                txtQtyOrderCS.Text += ".0";
            string[] cs_SingleQty = txtQtyOrderCS.Text.Split('.');

            var selectedProduct = db.Products.SingleOrDefault(s => s.ProdId == QuotationLineProductId);
            if (selectedProduct.csQty < Int32.Parse(cs_SingleQty[0])
                || (selectedProduct.unitQty + selectedProduct.csUnitQty) < Int32.Parse(cs_SingleQty[1]))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>" +
                    "alert('" + txtQtyOrderCS.Text + " Qty is not available into stocks. Please enter valid Qty.!!!')</SCRIPT>");
                //txtQtyOrder.Text = null;
                txtQtyOrderCS.Text = null;
                return;
            }

            int totalSingleEntryQty = ((int)selectedProduct.csUnitQty * (int)Int32.Parse(cs_SingleQty[0])) + (int)Int32.Parse(cs_SingleQty[1]);

            txtQtyOrder.Text = totalSingleEntryQty.ToString();
            txtQtyOrder_TextChanged(sender, e);
        }

        protected void txtContact_TextChanged(object sender, EventArgs e)
        {
            if (!isMobileValid(txtContact.Text))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>" +
                   "alert('" + txtContact.Text + " Invalid Caontact Number!!!')</SCRIPT>");
                txtContact.Text = null;
            }
        }

        // method containing the regex for mobile number valodation
        public static bool isMobileValid(string str)
        {
            string strRegex = @"^(0|91)?[6-9][0-9]{9}$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(str))
                return (true);
            else
                return (false);
        }
    }
}