﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class Index : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public int CategoryIDView = 0;
        public string TypeIDView = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "bl")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (!IsPostBack)
            {

                getProductGrid(CategoryIDView, TypeIDView);
                fillDdlProducteCategotyView();
                fillDdlProducteTypeView();
            }
        }

        public void fillDdlProducteCategotyView()
        {
            ddlProductCategoryView.Items.Clear();
            var cat = db.ProductCategories.Select(m => new { id = m.CatId, name = m.name }).ToList();


            ddlProductCategoryView.DataSource = cat;
            ddlProductCategoryView.DataTextField = "name";
            ddlProductCategoryView.DataValueField = "id";
            ddlProductCategoryView.DataBind();
            ddlProductCategoryView.Items.Insert(0, "Select");
        }
        public void fillDdlProducteTypeView()
        {
            ddlProductTypeView.Items.Clear();
            var cat = db.ProductTypes.Select(m => new { name = m.name }).Distinct().ToList();

            ddlProductTypeView.DataSource = cat;
            ddlProductTypeView.DataTextField = "name";
            ddlProductTypeView.DataValueField = "name";
            ddlProductTypeView.DataBind();
            ddlProductTypeView.Items.Insert(0, "Select");
        }
        protected void ddlProductCategoryView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewCategory();
            getProductGrid(CategoryIDView, TypeIDView);
        }

        private void getProductGrid(int catId, string typeId)
        {
            if (catId > 0)
            {
                var s = db.Products.Where(l => l.CatId == catId && l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty
                    TotalUnitQty = m.csUnitQty * m.csQty + m.unitQty,  // total singelunit qty
                    gst = m.gst
                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();

            }
            else if (typeId != "")
            {
                var s = db.Products.Where(l => l.ProductType.name == typeId && l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty
                    TotalUnitQty = m.csUnitQty * m.csQty + m.unitQty,  // total singelunit qty
                    gst = m.gst
                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
            else
            {
                var s = db.Products.Where(l => l.IsDeactive != true).Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty
                    TotalUnitQty = m.csUnitQty * m.csQty + m.unitQty,  // total singelunit qty
                    gst = m.gst

                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
        }
        private void selectedViewCategory() // gridview editing selcted viewcategory  
        {
            if (ddlProductCategoryView.SelectedIndex == 0)
                CategoryIDView = 0;
            else
                CategoryIDView = Int32.Parse(ddlProductCategoryView.SelectedItem.Value);

            ddlProductTypeView.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        private void filterGridSync() // for editiing, deleting and cancel selected filter syncronisiatin with grid
        {
            if (ddlProductTypeView.SelectedIndex > 0)
            {
                selectedViewType();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductCategoryView.SelectedIndex > 0)
            {
                selectedViewCategory();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductTypeView.SelectedIndex == 0 && ddlProductCategoryView.SelectedIndex == 0 && txtSearch.Text == "")
            {
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else
            {
                this.SearchText();
            }
        }
        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvProducts.PageIndex = e.NewPageIndex;
            filterGridSync();
        }

        private void selectedViewType() // gridview editing selcted viewType  
        {
            if (ddlProductTypeView.SelectedIndex == 0)
                TypeIDView = "";
            else
                TypeIDView = ddlProductTypeView.SelectedItem.Text;

            ddlProductCategoryView.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        protected void ddlProductTypeView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewType();
            getProductGrid(CategoryIDView, TypeIDView);
        }
        /// dynamic serach////
        /// 

        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable GetRecords()

        {
            var s = db.Products.Where(l => l.IsDeactive != true).Select(m => new
            {
                id = m.ProdId,
                CategoryName = m.ProductCategory.name,
                TypeName = m.ProductType.name,
                ProductName = m.name,
                UnitPrice = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                Qty = m.csQty,// box qty
                Unit = m.unit,
                csUnitQty = m.csUnitQty, // in box no of Item
                UnitQty = m.unitQty,  // broken box no of item // Single Qty
                TotalUnitQty = m.csUnitQty * m.csQty + m.unitQty,  // total singelunit qty
                gst = m.gst

            })
                .OrderBy(k => k.CategoryName).ToList();

            gvProducts.DataSource = s;
            gvProducts.DataBind();

            DataTable dt = LINQResultToDataTable(s);

            return dt;
        }
        private void BindGrid()
        {
            DataTable dt = GetRecords();
            if (dt.Rows.Count > 0)
            {
                gvProducts.DataSource = dt;
                gvProducts.DataBind();
            }
        }

        private void SearchText()
        {
            DataTable dt = GetRecords();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvProducts.SortExpression, txtSearch.Text);
            }

            if (SearchExpression != null)
            {
                dv.RowFilter = "ProductName like" + SearchExpression;
                gvProducts.DataSource = dv;
                gvProducts.DataBind();
            }
        }

        public string Highlight(string InputTxt)
        {
            string Search_Str = txtSearch.Text.ToString();
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(),
            RegexOptions.IgnoreCase);

            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt,
            new MatchEvaluator(ReplaceKeyWords));

            // Set the RegExp to null.
            RegExp = null;
        }

        public string ReplaceKeyWords(Match m)
        {
            return "<span class='highlight'>" + m.Value + "</span>";

        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchText();
            ddlProductCategoryView.SelectedIndex = -1;
        }
    }
}