﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Billing.aspx.cs" Inherits="QuickStoreBilling.Billing" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="TabName" runat="server" />
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Products </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sales</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">Product Select</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">Billing Product
                <asp:Label runat="server" ID="labBillNumber" Text="Bill No. " Visible="false" />
                <asp:Label runat="server" ID="labCountBillingItem" Text="0" Visible="false" /></span></a> </li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab2" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">Invoice</span></a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane p-20 active" id="tab0" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">
                    <%--<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvProdUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
                    <div class="form-group row mrg">
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad">Product Category</label>
                        <div class="col-sm-3 fev">
                            <asp:DropDownList ID="ddlProductCategoryView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlProductCategoryView_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Product Type</label>
                        <div class="col-sm-3 fev">
                            <asp:DropDownList ID="ddlProductTypeView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlProductTypeView_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
                    Search Products:<asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" />
                    <asp:Label runat="server" ID="labCat" Text="" Visible="false" />
                    <asp:GridView ID="gvProducts" runat="server" DataKeyNames="id" CssClass="GridViewEditRow"
                        EmptyDataText="No records has been added."
                        AutoGenerateColumns="False" Width="100%" ShowFooter="True" BackColor="White" BorderColor="#999999" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical"
                        OnPageIndexChanging="gvProducts_PageIndexChanging" PageSize="25" AllowPaging="True" EnableTheming="True">
                        <AlternatingRowStyle BackColor="#CCCCCC" />
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                            <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                            <asp:TemplateField HeaderText="[c/s] . [Lose] Stock" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <%--<asp:Label ID="Label1" runat="server" Text='<%# Bind("BoxQty","{0:n}") %>'></asp:Label>(Box)
                                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("InBoxNoOfQty","{0:n}") %>'></asp:Label>(InBoxQty)
                                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("SingleQty","{0:n}") %>'></asp:Label>(LoseQty)--%>
                                    <asp:Label ID="labBoxDotSingleQty" runat="server" Text='<%# Bind("BoxQty") %>'></asp:Label>
                                    .
                                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("LoseQty") %>'></asp:Label>= 
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Qty","{0:0}") %>' ForeColor="blue"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UnitPrice<br/>Inc. All" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labUnitPrice" runat="server" Text='<%# Bind("UnitPrice","{0:n}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="mrp" HeaderText="M.R.P" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                                    <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            <asp:TemplateField HeaderText="[c/s] . [Single] =Qty" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtEntryByCS" runat="server" OnTextChanged="txtEntryByCS_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <asp:TextBox ID="txtQty" runat="server" OnTextChanged="txtQty_TextChanged" AutoPostBack="true" ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labTotal" runat="server" DataFormatString="{0:n}"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton Visible="false" ID="lbAddToBill" runat="server" OnClick="lbAddToBill_Click" Style="padding: 0;" CssClass="m-icon"><i style="font-size: 20px;color: #2eb2da;margin-right: 0;" class="m-r-10 mdi mdi-plus-circle"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CategoryName" HeaderText="Product Category" Visible="false" />
                        </Columns>
                        <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerSettings PreviousPageText="Previous" NextPageText="Next" FirstPageText="First" LastPageText="Last" PageButtonCount="10" />
                        <PagerStyle CssClass="Pager" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>
                    <%--</ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvProducts" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                </div>
            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">

                    <%--<label class="col-md-3 m-t-15">Product Billing</label>--%>

                    <%--<asp:UpdatePanel ID="updTimeSheet" UpdateMode="Always" runat="server">
                    <ContentTemplate>--%>
                    <asp:Panel ID="pnl_warning" runat="server" Visible="false">
                        <div class="alert alert-warning" role="alert" class="col-sm-2 fev pad">
                            <asp:Label ID="labWarning" runat="server" Text=""></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnl_err" runat="server" Visible="false">
                        <div class="alert alert-danger" role="alert" class="col-sm-2 fev pad">
                            <asp:Label ID="LabErr" runat="server" Text=""></asp:Label>
                        </div>
                    </asp:Panel>
                    <div class="form-group row mrg">
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad"></label>
                        <div class="col-sm-3 fev">
                            <asp:CheckBox runat="server" ID="ChkBxNewCuromer" Text="_New Customer" Font-Bold="true" Checked="true" OnCheckedChanged="ChkBxNewCuromer_CheckedChanged" AutoPostBack="true"/>
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Existing Customers:</label>
                        <div class="col-sm-3 fev">
                            <asp:DropDownList ID="ddlCustomers" runat="server" class="select2 form-control custom-select" Width="350px" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" />
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Customer Name:</label>
                        <div class="col-sm-3 fev">
                            <asp:TextBox ID="txtCustomerName" class="form-control" runat="server" />
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad">Contact Number:</label>
                        <div class="col-sm-3 fev">
                            <asp:TextBox ID="txtContact" runat="server" class="form-control" OnTextChanged="txtContact_TextChanged" AutoPostBack="true" />
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Date:</label>
                        <div class="col-sm-3 fev">
                            <asp:TextBox ID="txtDate" runat="server" class="form-control" TextMode="Date" />
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad">Address:</label>
                        <div class="col-sm-3 fev">
                            <asp:TextBox ID="txtAddress" runat="server" class="form-control" TextMode="MultiLine" />
                        </div>
                        <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">GSTIN:</label>
                        <div class="col-sm-3 fev">
                            <asp:TextBox ID="txtGSTIN" class="form-control" runat="server" />
                        </div>
                        <asp:TextBox ID="TextBox3" runat="server" Visible="false" CssClass="col-sm-1 fev pad" />

                    </div>
                    <asp:GridView ID="gvOrder" runat="server" DataKeyNames="id" CssClass="GridViewEditRow"
                        EmptyDataText="No records has been added."
                        AutoGenerateColumns="False" Width="100%" ShowFooter="True" BackColor="White" BorderColor="#999999" BorderStyle="Solid"
                        BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical"
                        EnableTheming="True">
                        <AlternatingRowStyle BackColor="#CCCCCC" />
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                            <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                            <asp:TemplateField HeaderText="[c/s] . [Lose] Stock" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("BoxQty","{0:n}") %>' Visible="false"></asp:Label><%--(Box)--%>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("InBoxNoOfQty","{0:n}") %>' Visible="false"></asp:Label><%--(InBoxQty)--%>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("SingleQty","{0:n}") %>' Visible="false"></asp:Label><%--(LoseQty)--%>
                                    <asp:Label ID="labBoxDotSingleQty" runat="server" Text='<%# Bind("BoxDotSingleQty","{0:n}") %>'></asp:Label>= 
                                    <asp:Label ID="labTotalQtyOrder" runat="server" Text='<%# Bind("totalQty") %>' ForeColor="blue"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit<br/>Price" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labUnitPriceOrder" runat="server" Text='<%# Bind("UnitPrice","{0:n}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="GST<br/>(%)" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labGst" runat="server" Text='<%# Bind("gst","{0:n}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SinglePrice <br /> (Inc. All)" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labUnitPriceOrderAll" runat="server" Text='<%# Bind("UnitPriceAll","{0:n}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="mrp" HeaderText="M.R.P" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                                    <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            <asp:TemplateField HeaderText="[c/s] . [Single] =Qty" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtEntryByCSOrder" runat="server" Text='<%# Bind("cs_SingleQty") %>' OnTextChanged="txtEntryByCSOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <asp:TextBox ID="txtQtyOrder" runat="server" Text='<%# Bind("Qty","{0:n}") %>' OnTextChanged="txtQtyOrder_TextChanged" AutoPostBack="true" ReadOnly="true"></asp:TextBox>

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="GridHeader">
                                <ItemTemplate>
                                    <asp:Label ID="labTotalOrder" runat="server" Text='<%# Bind("TotalPrice","{0:n}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>

                        </Columns>
                        <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle CssClass="Pager" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>
                    <div class="" style="text-align: -webkit-right;">
                        <table style="text-align: right">
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="labTotal" Text="Total(Excl. GST)"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtTotalExclGst" Style="text-align: right" ReadOnly="true"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label3" Text="GST"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtGST" Style="text-align: right" ReadOnly="true"></asp:TextBox></td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="labFotterTotal" Text="Total"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtTotal" Style="text-align: right" ReadOnly="true"></asp:TextBox></td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label5" Text="Discount(%)"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtDiscount_" Text="0" placeholder="0%" Style="text-align: right" OnTextChanged="txtDiscount__TextChanged" AutoPostBack="true" Width="17%" />
                                    <asp:TextBox runat="server" ID="txtDiscount" Text="0" Style="text-align: right" ReadOnly="true" Width="25%"></asp:TextBox>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label6" Text="Net Total"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtNetTotal" Text="Net Total" Style="text-align: right" ReadOnly="true"></asp:TextBox></td>

                            </tr>
                        </table>
                    </div>
                    <%--</ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvProducts" EventName="RowCreated" />
                        <asp:AsyncPostBackTrigger ControlID="ddlCategory" EventName="OnSelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>--%>

                    <br />
                    <div class="border-top">
                        <div class="card-body" style="text-align: right;">
                            <asp:Button class="btn btn-primary" ID="btnQuoteToBillSubmit" runat="server" Text="Submit" OnClick="btnQuoteToBillSubmit_Click" />
                            <asp:Button class="btn btn-primary" ID="btnInvoice" runat="server" Text="Print Quotation" OnClick="btnInvoice_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="tab2" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">

                    <label class="col-md-3 m-t-15">Invoice</label>
                    <div class="table-responsive">
                        <%--<asp:UpdatePanel ID="updTimeSheet" UpdateMode="Always" runat="server">
                    <ContentTemplate>--%>
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <div class="alert alert-warning" role="alert" style="width: 100%">
                                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                            <div class="alert alert-danger" role="alert" style="width: 100%">
                                <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <div style="text-align: -moz-center; border-bottom-width: 1px; border-bottom-style: solid; border-top-width: 1.5px; border-top-style: solid; border-right-width: 1px; border-right-style: solid; border-left-width: 1px; border-left-style: solid; border-color: #b66dff;">
                            <table style="margin-top: 10px;">
                                <tr style="border-top-width: 2px; border-top-style: solid; border-right-width: 2px; border-right-style: solid; border-bottom-width: 2px; border-bottom-style: solid; border-left-width: 2px; border-left-style: solid; color: forestgreen;">
                                    <asp:Panel runat="server" ID="Panel3">
                                        <td style="text-align: right"><span style="font-weight: bold;">Existing Customers:</span>
                                            <asp:DropDownList ID="ddlInvoiceCustomer" runat="server" class="select2" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceCustomer_SelectedIndexChanged">
                                            </asp:DropDownList></td>
                                        <td style="text-align: right"><span style="font-weight: bold;">Bill No:</span>
                                            <asp:DropDownList ID="ddlInvoiceBillNo" runat="server" class="select2" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceBillNo_SelectedIndexChanged">
                                            </asp:DropDownList></td>

                                    </asp:Panel>


                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>

                            </table>
                        </div>

                        <asp:ScriptManager ID="scr" runat="server" ViewStateMode="Enabled"></asp:ScriptManager>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="" ClientIDMode="AutoID" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px"
                            ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226"
                            Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="600px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="">
                            <LocalReport ReportPath="Reports\OrderInvoice.rdlc">
                            </LocalReport>
                        </rsweb:ReportViewer>

                        <%--</ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvProducts" EventName="RowCreated" />
                        <asp:AsyncPostBackTrigger ControlID="ddlCategory" EventName="OnSelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>--%>
                    </div>



                </div>
            </div>
        </div>
    </div>
</asp:Content>
