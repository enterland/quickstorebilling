﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="QuickStoreBilling.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="TabName" runat="server" />
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Products </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div style="margin: 10px; overflow-x: auto">
            <%--<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvProdUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
            <div class="form-group row mrg">
                <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad">Product Category</label>
                <div class="col-sm-2 fev">
                    <asp:DropDownList ID="ddlProductCategoryView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlProductCategoryView_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Product Type</label>
                        <div class="col-sm-3 fev">
                            <asp:DropDownList ID="ddlProductTypeView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlProductTypeView_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            Search Products:<asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" />
            <asp:Label runat="server" ID="labCat" Text="" Visible="false" />
            <asp:GridView ID="gvProducts" runat="server" DataKeyNames="id"
                EmptyDataText="No records has been added."
                AutoGenerateColumns="False" Width="100%" ShowFooter="True"
                CellPadding="3"
                OnPageIndexChanging="gvProducts_PageIndexChanging" PageSize="25" AllowPaging="True" EnableTheming="True" 
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                    <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                    <asp:BoundField DataField="Unit" HeaderText="Unit" Visible="false" />
                    <asp:BoundField DataField="Qty" HeaderText="C/S Box <br/> Qty" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="csUnitQty" HeaderText="InC/S<br/>Box qty" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UnitQty" HeaderText="Lose<br/>Qty" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalUnitQty" HeaderText="Total<br/>Unit<br/>Qty" DataFormatString="{0:0}" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="gst" HeaderText="GST<br />(%)" HtmlEncode="false" HeaderStyle-CssClass="GridHeader">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UnitPrice" HeaderText="Unit<br/>Price" HtmlEncode="false" HeaderStyle-CssClass="GridHeader" DataFormatString="{0:n}">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CategoryName" HeaderText="Cat/<br/>Comp<br/>Name" HtmlEncode="false"/>
                    <asp:BoundField DataField="TypeName" HeaderText="Type" />
                </Columns>
                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerSettings PreviousPageText="Previous" NextPageText="Next" FirstPageText="First" LastPageText="Last" PageButtonCount="10" />
                <PagerStyle CssClass="Pager" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <%--</ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvProducts" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
        </div>
    </div>
</asp:Content>
