﻿using Microsoft.Reporting.WebForms;
using QuickStoreBilling.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class QuotationReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "bl")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (!IsPostBack)
            {

                getInvoiceReport();
            }
        }
        public void getInvoiceReport()
        {
            try
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Quotation.rdlc");

                QuotationDSClasss tr = new QuotationDSClasss();
                DataSet ds = new DataSet();

                //bool islv = bool.Parse(Request.QueryString["is"]);
                ds = tr.getQuotation();

                ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("QuotationDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}