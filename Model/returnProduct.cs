//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuickStoreBilling.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class returnProduct
    {
        public int retrnId { get; set; }
        public Nullable<int> orderId { get; set; }
        public Nullable<System.DateTime> retrnDate { get; set; }
        public string retrnCause { get; set; }
        public Nullable<bool> isRetrnApproved { get; set; }
        public Nullable<bool> isReturned { get; set; }
        public Nullable<bool> isRefund { get; set; }
    
        public virtual OrderLine OrderLine { get; set; }
    }
}
