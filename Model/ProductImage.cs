//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuickStoreBilling.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductImage
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductImage()
        {
            this.carts = new HashSet<cart>();
            this.OrderLines = new HashSet<OrderLine>();
        }
    
        public int ProdImgId { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public Nullable<bool> isCover { get; set; }
        public Nullable<int> ProdId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cart> carts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderLine> OrderLines { get; set; }
        public virtual Product Product { get; set; }
    }
}
