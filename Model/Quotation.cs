//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuickStoreBilling.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Quotation
    {
        public int quotationLineId { get; set; }
        public Nullable<int> uId { get; set; }
        public Nullable<int> prodId { get; set; }
        public string prodName { get; set; }
        public string size { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> unitPrice { get; set; }
        public Nullable<decimal> gst { get; set; }
        public Nullable<decimal> unitPriceAll { get; set; }
        public Nullable<decimal> totalPrice { get; set; }
        public Nullable<System.DateTime> quotationDate { get; set; }
        public Nullable<int> quotationId { get; set; }
    }
}
