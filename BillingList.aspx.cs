﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class BillingList : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public int CustomerIDView = 0;
        public string BillIDView = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "bl")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (!IsPostBack)
            {

                getProductGrid(CustomerIDView, BillIDView);
                fillDdlddlCustomerView();
            }
        }
        public void fillDdlddlCustomerView()
        {
            ddlCustomerView.Items.Clear();
            var cat = db.users.Where(l => l.userCatId == 2).Select(m => new { id = m.uId, name = m.name }).ToList();


            ddlCustomerView.DataSource = cat;
            ddlCustomerView.DataTextField = "name";
            ddlCustomerView.DataValueField = "id";
            ddlCustomerView.DataBind();
            ddlCustomerView.Items.Insert(0, "Select");
        }
        public void fillDdlBillView(int uid)
        {
            ddlBillView.Items.Clear();
            var bil = db.Orders.Where(s => s.uid == uid).Select(m => new { id = m.orderId }).ToList();



            ddlBillView.DataSource = bil;
            ddlBillView.DataTextField = "id";
            ddlBillView.DataValueField = "id";
            ddlBillView.DataBind();
            ddlBillView.Items.Insert(0, "Select");
        }
        private void getProductGrid(int customerId, string billId)
        {
            if (customerId > 0)
            {
                var s = db.Orders.Where(l => l.uid == customerId).Select(m => new
                {
                    id = m.orderId,
                    BillNumber = m.orderId,
                    CustomerName = m.user.name,
                    OrderSingleUnitQty = "",
                    TotalBillAmount = m.billTotal,
                    BillDate = m.billDate,
                    Contact = m.address.MobileNo

                })
              .OrderBy(k => k.BillNumber).ToList();
                gvBillingList.DataSource = s;
                gvBillingList.DataBind();

            }
            else if (billId != "")
            {
                var s = db.Orders.Where(l => l.orderId == billId).Select(m => new
                {
                    id = m.orderId,
                    BillNumber = m.orderId,
                    CustomerName = m.user.name,
                    OrderSingleUnitQty = "",
                    TotalBillAmount = m.billTotal,
                    BillDate = m.billDate,
                    Contact = m.address.MobileNo

                })
               .OrderBy(k => k.BillNumber).ToList();
                gvBillingList.DataSource = s;
                gvBillingList.DataBind();
            }
            else
            {
                var s = db.Orders.Select(m => new
                {
                    id = m.orderId,
                    BillNumber = m.orderId,
                    CustomerName = m.user.name,
                    OrderSingleUnitQty = "",
                    TotalBillAmount = m.billTotal,
                    BillDate = m.billDate,
                    Contact = m.address.MobileNo

                })
                .OrderBy(k => k.BillNumber).ToList();
                gvBillingList.DataSource = s;
                gvBillingList.DataBind();
            }
        }
        private void selectedViewCustomer() // gridview editing selcted viewcategory  
        {
            if (ddlCustomerView.SelectedIndex == 0)
                CustomerIDView = 0;
            else
                CustomerIDView = Int32.Parse(ddlCustomerView.SelectedItem.Value);

            fillDdlBillView(CustomerIDView);
            txtSearch.Text = "";
        }
        private void selectedViewBill() // gridview editing selcted viewType  
        {
            if (ddlBillView.SelectedIndex == 0)
                BillIDView = "";
            else
                BillIDView = ddlBillView.SelectedItem.Text;

            txtSearch.Text = "";
        }
        protected void ddlCustomerView_SelectedIndexChanged(object sender, EventArgs e)
        {

            selectedViewCustomer();
            getProductGrid(CustomerIDView, BillIDView);
        }

        protected void ddlBillView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewBill();
            getProductGrid(CustomerIDView, BillIDView);
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchText();
            ddlCustomerView.SelectedIndex = -1;
        }
        private void filterGridSync() // for editiing, deleting and cancel selected filter syncronisiatin with grid
        {
            if (ddlBillView.SelectedIndex > 0)
            {
                selectedViewBill();
                this.getProductGrid(CustomerIDView, BillIDView);
            }
            else if (ddlCustomerView.SelectedIndex > 0)
            {
                selectedViewCustomer();
                this.getProductGrid(CustomerIDView, BillIDView);
            }
            else if (ddlBillView.SelectedIndex == 0 && ddlCustomerView.SelectedIndex == 0 && txtSearch.Text == "")
            {
                this.getProductGrid(CustomerIDView, BillIDView);
            }
            else
            {
                this.SearchText();
            }
        }
        protected void gvBillingList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBillingList.PageIndex = e.NewPageIndex;
            filterGridSync();
        }

        /// dynamic serach////
        /// 

        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable GetRecords()

        {
            var s = db.Orders.Select(m => new
            {
                id = m.orderId,
                BillNumber = m.orderId,
                CustomerName = m.user.name,
                OrderSingleUnitQty = "",
                TotalBillAmount = m.billTotal,
                BillDate = m.billDate,
                Contact = m.address.MobileNo

            })
                .OrderBy(k => k.BillNumber).ToList();
            gvBillingList.DataSource = s;
            gvBillingList.DataBind();

            DataTable dt = LINQResultToDataTable(s);

            return dt;
        }
        private void BindGrid()
        {
            DataTable dt = GetRecords();
            if (dt.Rows.Count > 0)
            {
                gvBillingList.DataSource = dt;
                gvBillingList.DataBind();
            }
        }

        private void SearchText()
        {
            DataTable dt = GetRecords();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvBillingList.SortExpression, txtSearch.Text);
            }

            try
            {
                if (SearchExpression != null)
                {
                    dv.RowFilter = "CustomerName like" + SearchExpression;
                    gvBillingList.DataSource = dv;
                    gvBillingList.DataBind();
                }
            }
            catch (Exception Ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + Ex.Message + "')</SCRIPT>");
            }
        }

        public string Highlight(string InputTxt)
        {
            string Search_Str = txtSearch.Text.ToString();
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(),
            RegexOptions.IgnoreCase);

            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt,
            new MatchEvaluator(ReplaceKeyWords));

            // Set the RegExp to null.
            RegExp = null;
        }

        public string ReplaceKeyWords(Match m)
        {
            return "<span class='highlight'>" + m.Value + "</span>";

        }

        protected void LinkButtonBillNUmber_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "openModal", "window.open('/BillingCard.aspx?o=" + lb.Text + "' ,'_blank');", true);

        }
    }
}