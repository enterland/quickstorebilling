﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BillingCard.aspx.cs" Inherits="QuickStoreBilling.BillingCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Billing Card </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sales Order</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div style="margin: 10px; overflow-x: auto">
            <%--<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvProdUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
            <div class="table-responsive" style="text-align: -moz-center; border-bottom-width: 1px; border-bottom-style: solid; border-top-width: 1.5px; border-top-style: solid; border-right-width: 1px; border-right-style: solid; border-left-width: 1px; border-left-style: solid; border-color: #b66dff;">
                <asp:DataList ID="DataListCustomerHeader" runat="server" Width="100%">
                    <ItemTemplate>
                        <table class="w-100">
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Bill No. :</td>
                                <td>
                                    <asp:TextBox ID="txtBillNo" runat="server" Text='<%# Bind("BillNo") %>' ReadOnly="true"></asp:TextBox></td>
                                <td>Order date :</td>
                                <td>
                                    <asp:TextBox ID="txtOrdateDate" runat="server" Text='<%# Bind("OrderDate") %>' ReadOnly="true"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Customer Name : </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerName" runat="server" Text='<%# Bind("CustomerName") %>' ReadOnly="true"></asp:TextBox></td>
                                <td>Contact No. : </td>
                                <td>
                                    <asp:TextBox ID="txtContact" runat="server" Text='<%# Bind("Contact") %>' ReadOnly="true"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Address : </td>
                                <td>
                                    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Text='<%# Bind("Address") %>' ReadOnly="true"></asp:TextBox></td>
                                <td>
                                    </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </div>

            <asp:Panel runat="server" ID="pnlOrderLineAndHeader">
                <asp:GridView ID="gvOrder" runat="server" DataKeyNames="id"
                    EmptyDataText="No records has been added."
                    AutoGenerateColumns="False" Width="100%" ShowFooter="True" BackColor="White" BorderColor="#999999" BorderStyle="Solid"
                    BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical"
                    EnableTheming="True">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                        <asp:TemplateField HeaderText="UnitPrice" HeaderStyle-CssClass="GridHeader">
                            <ItemTemplate>
                                <asp:Label ID="labUnitPriceOrder" runat="server" Text='<%# Bind("UnitPrice","{0:n}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GST<br/>(%)" HeaderStyle-CssClass="GridHeader">
                            <ItemTemplate>
                                <asp:Label ID="labGst" runat="server" Text='<%# Bind("gst","{0:n}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="UnitPrice<br/>Incl. All" HeaderStyle-CssClass="GridHeader">
                            <ItemTemplate>
                                <asp:Label ID="labUnitPriceOrderAll" runat="server" Text='<%# Bind("UnitPriceAll","{0:n}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Qty" HeaderStyle-CssClass="GridHeader">
                            <ItemTemplate>
                                <asp:TextBox ID="txtQtyOrder" CssClass="form-control" style="text-align:right" runat="server" Text='<%# Bind("Qty","{0:n}") %>' ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="GridHeader">
                            <ItemTemplate>
                                <asp:Label ID="labTotalOrder" runat="server" Text='<%# Bind("TotalPrice","{0:n}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>

                    </Columns>
                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle CssClass="Pager" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <%--</ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvProducts" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                <div class="table-responsive" style="text-align: -moz-center; border-bottom-width: 1px; border-bottom-style: solid; border-top-width: 1.5px; border-top-style: solid; border-right-width: 1px; border-right-style: solid; border-left-width: 1px; border-left-style: solid; border-color: #b66dff;">
                    <asp:DataList ID="DataListOrderFooter" runat="server" Width="100%">
                        <ItemTemplate>
                            <table class="w-100">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>GST Amount :</td>
                                    <td>
                                        <asp:TextBox ID="txtGSTAmount" runat="server" Text='<%# Bind("GSTAmount") %>' ReadOnly="true"></asp:TextBox></td>
                                    <td>Total :</td>
                                    <td>
                                        <asp:TextBox ID="txtOrdateDate" runat="server" Text='<%# Bind("OrderTotal") %>' ReadOnly="true"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Discount : </td>
                                    <td>
                                        <asp:TextBox ID="txtTotalDiscount" runat="server" Text='<%# Bind("TotalDiscount") %>' ReadOnly="true"></asp:TextBox></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>GSTIN : </td>
                                    <td>
                                        <asp:TextBox ID="txtGSTIN" runat="server" Text='<%# Bind("GSTIN") %>' ReadOnly="true"></asp:TextBox></td>
                                    <td><asp:Button class="btn btn-primary" ID="buttonDelete" runat="server" Text="Cancel Order" OnClick="buttonDelete_Click" /> &nbsp;&nbsp;
                                        <asp:Button class="btn btn-primary" ID="buttonPrintBill" runat="server" Text="Print Bill" OnClick="buttonPrintBill_Click" />
                                        </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlOrderCancelled" Visible="false">
                <h1 style="font-size:xx-large;color:red;text-align:center">Order was Cancelled !!!</h1>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
