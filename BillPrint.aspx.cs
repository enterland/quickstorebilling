﻿using Microsoft.Reporting.WebForms;
using QuickStoreBilling.Model;
using QuickStoreBilling.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class BillPrint : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() == "sa")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }

            
            if (!IsPostBack)
            {

                getInvoiceReport(Request.QueryString["o"].ToString());
            }
        }
        public void getInvoiceReport(string orderId)
        {
            try
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/OrderInvoice.rdlc");

                InvoiceDSClasss tr = new InvoiceDSClasss();
                DataSet ds = new DataSet();

                //bool islv = bool.Parse(Request.QueryString["is"]);

                Order order = db.Orders.SingleOrDefault(l => l.orderId == orderId);

                ds = tr.getInvoice((int)order.uid, orderId);

                ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("InvoiceDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}