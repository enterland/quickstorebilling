﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling.Admin
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public int Category = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetInitialRow();
            }
        }

        private void FillDropDownListCategory(DropDownList ddl)
        {
            var cat = db.ProductCategories.Select(m => new { id = m.CatId, name = m.name }).ToList();


            ddl.DataSource = cat;
            ddl.DataTextField = "name";
            ddl.DataValueField = "id";
            ddl.DataBind();
            // ddl.Items.Insert(0, "Select");

        }
        private void FillDropDownListType(DropDownList ddl)
        {
            ddl.Items.Clear();
            if (Category != 0)
            {

                var tp = db.ProductTypes.Where(l => l.ProductCatId == Category).Select(m => new { id = m.ProductTypeId, name = m.name }).ToList();

                ddl.DataSource = tp;
                ddl.DataTextField = "id";
                ddl.DataValueField = "name";
                ddl.DataBind();
                //ddl.Items.Insert(0, "Select");

            }
        }

        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Category", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));
            dt.Columns.Add(new DataColumn("Price", typeof(string)));
            dt.Columns.Add(new DataColumn("Qty", typeof(string)));


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Name"] = string.Empty;
            dr["Description"] = string.Empty;
            dr["Price"] = string.Empty;
            dr["Qty"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            gvProducts.DataSource = dt;
            gvProducts.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)gvProducts.Rows[0].Cells[2].FindControl("ddlCategory");

            FillDropDownListCategory(ddl1);



        }


        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                                //extract the TextBox values   

                                TextBox box1 = (TextBox)gvProducts.Rows[i].Cells[1].FindControl("txtName");
                                TextBox box2 = (TextBox)gvProducts.Rows[i].Cells[4].FindControl("txtDescription");
                                TextBox box3 = (TextBox)gvProducts.Rows[i].Cells[5].FindControl("txtPrice");
                                TextBox box4 = (TextBox)gvProducts.Rows[i].Cells[6].FindControl("txtQty");


                                dtCurrentTable.Rows[i]["Name"] = box1.Text;
                                dtCurrentTable.Rows[i]["Description"] = box2.Text;
                                dtCurrentTable.Rows[i]["Price"] = box3.Text;
                                dtCurrentTable.Rows[i]["Qty"] = box4.Text;

                                //extract the DropDownList Selected Items   

                                DropDownList ddl1 = (DropDownList)gvProducts.Rows[i].Cells[2].FindControl("ddlCategory");

                                // Update the DataRow with the DDL Selected Items   

                                dtCurrentTable.Rows[i]["Category"] = ddl1.SelectedItem.Text;

                                if (ddl1.SelectedItem.Text != "Select")
                                {
                                    ProductCategory c = db.ProductCategories.Where(l => l.name == ddl1.SelectedItem.Text).FirstOrDefault();
                                    Category = c.CatId;
                                }


                                DropDownList ddl2 = (DropDownList)gvProducts.Rows[i].Cells[3].FindControl("ddlType");
                                dtCurrentTable.Rows[i]["Type"] = ddl2.SelectedItem.Text;
                            }

                            //Rebind the Grid with the current data to reflect changes   
                            gvProducts.DataSource = dtCurrentTable;
                            gvProducts.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)gvProducts.Rows[i].Cells[1].FindControl("txtName");
                            TextBox box2 = (TextBox)gvProducts.Rows[i].Cells[4].FindControl("txtDescription");
                            TextBox box3 = (TextBox)gvProducts.Rows[i].Cells[5].FindControl("txtPrice");
                            TextBox box4 = (TextBox)gvProducts.Rows[i].Cells[6].FindControl("txtQty");


                            DropDownList ddl1 = (DropDownList)gvProducts.Rows[rowIndex].Cells[2].FindControl("ddlCategory");
                            DropDownList ddl2 = (DropDownList)gvProducts.Rows[rowIndex].Cells[3].FindControl("ddlType");

                            //Fill the DropDownList with Data   
                            FillDropDownListCategory(ddl1);

                            int iu = ddl1.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   
                                box1.Text = dt.Rows[i]["Name"].ToString();
                                box2.Text = dt.Rows[i]["Description"].ToString();
                                box3.Text = dt.Rows[i]["Price"].ToString();
                                box4.Text = dt.Rows[i]["Qty"].ToString();

                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                ddl1.ClearSelection();
                                ddl1.Items.FindByText(dt.Rows[i]["Category"].ToString()).Selected = true;
                                if (ddl1.SelectedItem.Text != "Select")
                                {
                                    ProductCategory c = db.ProductCategories.Where(l => l.name == ddl1.SelectedItem.Text).FirstOrDefault();
                                    Category = c.CatId;
                                }
                                
                                FillDropDownListType(ddl2);

                                ddl2.ClearSelection();
                                //int iue = ddl2.Items.Count;
                                //ddl2.Items.FindByText(dt.Rows[i]["Type"].ToString()).Selected = true;

                                //// Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
                                //pnl_warning.Visible = true;
                                //btnSubmitTimeSheet.Visible = true;

                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            if (ddl.SelectedItem.Text != "Select")
            {
                ProductCategory c = db.ProductCategories.Where(l => l.name == ddl.SelectedItem.Text).FirstOrDefault();
                Category = c.CatId;
            }
            

            GridViewRow row = (GridViewRow)ddl.NamingContainer;


            DropDownList ddl2 = (DropDownList)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("ddlType");
            //Fill the DropDownList with Data   

            FillDropDownListType(ddl2);

        }

        protected void lbDel_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                gvProducts.DataSource = dt;
                gvProducts.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }
        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }
        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("lbDel");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void lbAddNewRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }
        public void AddInitProduct()
        {

        }
    }
}