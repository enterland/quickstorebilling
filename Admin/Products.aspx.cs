﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickStoreBilling.Model;
using EntityFramework.Extensions;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.RegularExpressions;

namespace QuickStoreBilling.Admin
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public int Category = 0;
        public int CategoryIDView = 0;
        public string TypeIDView = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            string userType = Session["user"].ToString();
            if (userType == "bl")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }

            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
            if (!IsPostBack)
            {

                getProductGrid(CategoryIDView, TypeIDView);
                SetInitialRow();
                fillDdlProducteCategotyView();
                fillDdlProducteTypeView();
            }
        }

        public void fillDdlProducteCategotyView()
        {
            ddlProductCategoryView.Items.Clear();
            var cat = db.ProductCategories.Select(m => new { id = m.CatId, name = m.name }).ToList();


            ddlProductCategoryView.DataSource = cat;
            ddlProductCategoryView.DataTextField = "name";
            ddlProductCategoryView.DataValueField = "id";
            ddlProductCategoryView.DataBind();
            ddlProductCategoryView.Items.Insert(0, "Select");
        }
        public void fillDdlProducteTypeView()
        {
            ddlProductTypeView.Items.Clear();
            var cat = db.ProductTypes.Select(m => new { name = m.name }).Distinct().ToList();

            ddlProductTypeView.DataSource = cat;
            ddlProductTypeView.DataTextField = "name";
            ddlProductTypeView.DataValueField = "name";
            ddlProductTypeView.DataBind();
            ddlProductTypeView.Items.Insert(0, "Select");
        }
        private void getProductGrid(int catId, string typeId)
        {

            if (catId > 0)
            {
                var s = db.Products.Where(l => l.CatId == catId).Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    Description = m.description,
                    Price = m.price,
                    SingleUnitPrice = (m.price / m.csUnitQty),
                    Gst = m.gst,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty // lose Item qty
                    Margin = m.sellingMargin,
                    gst = m.gst,
                    PriceAll = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    ProductDeactive = m.IsDeactive,
                    mrp=m.mrp
                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();

            }
            else if (typeId != "")
            {
                var s = db.Products.Where(p => p.ProductType.name == typeId).Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    Description = m.description,
                    Price = m.price,
                    SingleUnitPrice = (m.price / m.csUnitQty),
                    Gst = m.gst,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty //lose item qty
                    Margin = m.sellingMargin,
                    gst = m.gst,
                    PriceAll = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    ProductDeactive = m.IsDeactive,
                    mrp = m.mrp
                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
            else
            {
                var s = db.Products.Select(m => new
                {
                    id = m.ProdId,
                    CategoryName = m.ProductCategory.name,
                    TypeName = m.ProductType.name,
                    ProductName = m.name,
                    Description = m.description,
                    Price = m.price,
                    SingleUnitPrice = (m.price / m.csUnitQty),
                    Gst = m.gst,
                    Qty = m.csQty,// box qty
                    Unit = m.unit,
                    csUnitQty = m.csUnitQty, // in box no of Item
                    UnitQty = m.unitQty,  // broken box no of item // Single Qty //lose item qty
                    Margin = m.sellingMargin,
                    gst = m.gst,
                    PriceAll = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                    ProductDeactive = m.IsDeactive,
                    mrp = m.mrp
                })
                .OrderBy(k => k.CategoryName).ToList();
                gvProducts.DataSource = s;
                gvProducts.DataBind();
            }
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && gvProducts.EditIndex == e.Row.RowIndex)
            {

                DropDownList ddl1 = (DropDownList)e.Row.FindControl("DropDownList3");
                FillDropDownListCategory(ddl1);

                string selectedCat = DataBinder.Eval(e.Row.DataItem, "CategoryName").ToString();
                ddl1.Items.FindByText(selectedCat).Selected = true;

                DropDownList ddl2 = (DropDownList)e.Row.FindControl("DropDownList4");

                int ddl1select = Int32.Parse(ddl1.SelectedValue);

                var tp = db.ProductTypes.Where(l => l.ProductCatId == ddl1select).Select(m => new { id = m.ProductTypeId, name = m.name }).ToList();

                ddl2.DataSource = tp;
                ddl2.DataTextField = "name";
                ddl2.DataValueField = "id";
                ddl2.DataBind();

                string selectedType = DataBinder.Eval(e.Row.DataItem, "TypeName").ToString();
                ddl2.Items.FindByText(selectedType).Selected = true;

            }

            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProducts.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        private void filterGridSync() // for editiing, deleting and cancel selected filter syncronisiatin with grid
        {
            if (ddlProductTypeView.SelectedIndex > 0)
            {
                selectedViewType();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductCategoryView.SelectedIndex > 0)
            {
                selectedViewCategory();
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else if (ddlProductTypeView.SelectedIndex == 0 && ddlProductCategoryView.SelectedIndex == 0 && txtSearch.Text == "")
            {
                this.getProductGrid(CategoryIDView, TypeIDView);
            }
            else
            {
                this.SearchText();
            }
        }
        protected void gvProducts_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TabName.Value = "tab0";
            gvProducts.EditIndex = e.NewEditIndex;

            filterGridSync();

            GridViewRow row = gvProducts.Rows[e.NewEditIndex];

        }

        private void selectedViewCategory() // gridview editing selcted viewcategory  
        {
            if (ddlProductCategoryView.SelectedIndex == 0)
                CategoryIDView = 0;
            else
                CategoryIDView = Int32.Parse(ddlProductCategoryView.SelectedItem.Value);

            ddlProductTypeView.SelectedIndex = 0;
            txtSearch.Text = "";
        }

        private void selectedViewType() // gridview editing selcted viewType  
        {
            if (ddlProductTypeView.SelectedIndex == 0)
                TypeIDView = "";
            else
                TypeIDView = ddlProductTypeView.SelectedItem.Text;

            ddlProductCategoryView.SelectedIndex = 0;
            txtSearch.Text = "";
        }
        protected void gvProducts_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TabName.Value = "tab0";
            gvProducts.EditIndex = -1;
            filterGridSync();
        }

        protected void gvProducts_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TabName.Value = "tab0";
            GridViewRow row = gvProducts.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);

            int CatId = Int32.Parse(((DropDownList)row.FindControl("DropDownList3")).SelectedValue);
            ProductCategory cat = db.ProductCategories.Single(l => l.CatId == CatId);

            int typeId = Int32.Parse(((DropDownList)row.FindControl("DropDownList4")).SelectedValue);
            ProductType t = db.ProductTypes.Single(l => l.ProductTypeId == typeId);

            string Name = (row.Cells[3].Controls[0] as TextBox).Text;
            string Unit = (row.Cells[4].Controls[0] as TextBox).Text;
            string Qty = (row.Cells[5].Controls[0] as TextBox).Text;
            string CsUnitQty = (row.Cells[6].Controls[0] as TextBox).Text;
            string UnitQty = (row.Cells[7].Controls[0] as TextBox).Text;
            string Price = (row.Cells[8].Controls[0] as TextBox).Text;
            string Margin = (row.Cells[10].Controls[0] as TextBox).Text;
            string Gst = (row.Cells[11].Controls[0] as TextBox).Text;
            string Mrp = (row.Cells[13].Controls[0] as TextBox).Text;
            string Description = (row.Cells[15].Controls[0] as TextBox).Text;
            bool productDeactive = (row.Cells[16].Controls[1] as CheckBox).Checked;

            Product m = db.Products.Single(s => s.ProdId == id);

            m.CatId = cat.CatId;
            m.TypeId = t.ProductTypeId;
            m.name = Name.ToUpper();
            m.description = Description;
            m.price = Decimal.Parse(Price);
            if (Gst != null)
                m.gst = Decimal.Parse(Gst);
            m.csQty = Decimal.Parse(Qty);
            m.unit = Unit;
            if (CsUnitQty != "")
                m.csUnitQty = Decimal.Parse(CsUnitQty);
            if (UnitQty != "")
                m.unitQty = Int32.Parse(UnitQty);
            if (Margin != "")
                m.sellingMargin = Convert.ToDecimal(Margin);
            m.IsDeactive = productDeactive;
            m.mrp = Decimal.Parse(Mrp);

            db.SaveChanges();


            gvProducts.EditIndex = -1;
            filterGridSync();
        }

        protected void gvProducts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TabName.Value = "tab0";
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);
            db.Products.Where(s => s.ProdId == id).Delete();
            filterGridSync();
        }
        //##################################################### New tab code ##################################################################
        private void FillDropDownListCategory(DropDownList ddl)
        {
            var cat = db.ProductCategories.Select(m => new { id = m.CatId, name = m.name }).ToList();


            ddl.DataSource = cat;
            ddl.DataTextField = "name";
            ddl.DataValueField = "id";
            ddl.DataBind();
            // ddl.Items.Insert(0, "Select");

        }
        private void FillDropDownListType(DropDownList ddl)
        {
            ddl.Items.Clear();
            if (Category != 0)
            {

                var tp = db.ProductTypes.Where(l => l.ProductCatId == Category).Select(m => new { id = m.ProductTypeId, name = m.name }).ToList();

                ddl.DataSource = tp;
                ddl.DataTextField = "name";
                ddl.DataValueField = "id";
                ddl.DataBind();
                //ddl.Items.Insert(0, "Select");

            }
        }

        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Category", typeof(string)));
            dt.Columns.Add(new DataColumn("Type", typeof(string)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));
            dt.Columns.Add(new DataColumn("Price", typeof(string)));
            dt.Columns.Add(new DataColumn("csBoxQty", typeof(string)));


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Name"] = string.Empty;
            dr["Description"] = string.Empty;
            dr["Price"] = string.Empty;
            dr["csBoxQty"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            gvProductsNew.DataSource = dt;
            gvProductsNew.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)gvProductsNew.Rows[0].Cells[2].FindControl("ddlCategory");

            FillDropDownListCategory(ddl1);



        }


        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                                //extract the TextBox values   

                                TextBox box1 = (TextBox)gvProductsNew.Rows[i].Cells[1].FindControl("txtName");
                                TextBox box2 = (TextBox)gvProductsNew.Rows[i].Cells[4].FindControl("txtDescription");
                                TextBox box3 = (TextBox)gvProductsNew.Rows[i].Cells[5].FindControl("txtPrice");
                                TextBox box4 = (TextBox)gvProductsNew.Rows[i].Cells[6].FindControl("txtQty");


                                dtCurrentTable.Rows[i]["Name"] = box1.Text;
                                dtCurrentTable.Rows[i]["Description"] = box2.Text;
                                dtCurrentTable.Rows[i]["Price"] = box3.Text;
                                dtCurrentTable.Rows[i]["csBoxQty"] = box4.Text;

                                //extract the DropDownList Selected Items   

                                DropDownList ddl1 = (DropDownList)gvProductsNew.Rows[i].Cells[2].FindControl("ddlCategory");

                                // Update the DataRow with the DDL Selected Items   

                                dtCurrentTable.Rows[i]["Category"] = ddl1.SelectedItem.Text;

                                if (ddl1.SelectedItem.Text != "Select")
                                {
                                    ProductCategory c = db.ProductCategories.Where(l => l.name == ddl1.SelectedItem.Text).FirstOrDefault();
                                    Category = c.CatId;
                                }


                                DropDownList ddl2 = (DropDownList)gvProductsNew.Rows[i].Cells[3].FindControl("ddlType");
                                dtCurrentTable.Rows[i]["Type"] = ddl2.SelectedItem.Text;
                            }

                            //Rebind the Grid with the current data to reflect changes   
                            gvProductsNew.DataSource = dtCurrentTable;
                            gvProductsNew.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)gvProductsNew.Rows[i].Cells[1].FindControl("txtName");
                            TextBox box2 = (TextBox)gvProductsNew.Rows[i].Cells[4].FindControl("txtDescription");
                            TextBox box3 = (TextBox)gvProductsNew.Rows[i].Cells[5].FindControl("txtPrice");
                            TextBox box4 = (TextBox)gvProductsNew.Rows[i].Cells[6].FindControl("txtQty");


                            DropDownList ddl1 = (DropDownList)gvProductsNew.Rows[rowIndex].Cells[2].FindControl("ddlCategory");
                            DropDownList ddl2 = (DropDownList)gvProductsNew.Rows[rowIndex].Cells[3].FindControl("ddlType");

                            //Fill the DropDownList with Data   
                            FillDropDownListCategory(ddl1);

                            int iu = ddl1.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   
                                box1.Text = dt.Rows[i]["Name"].ToString();
                                box2.Text = dt.Rows[i]["Description"].ToString();
                                box3.Text = dt.Rows[i]["Price"].ToString();
                                box4.Text = dt.Rows[i]["csBoxQty"].ToString();

                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                ddl1.ClearSelection();
                                ddl1.Items.FindByText(dt.Rows[i]["Category"].ToString()).Selected = true;
                                if (ddl1.SelectedItem.Text != "Select")
                                {
                                    ProductCategory c = db.ProductCategories.Where(l => l.name == ddl1.SelectedItem.Text).FirstOrDefault();
                                    Category = c.CatId;
                                }

                                FillDropDownListType(ddl2);

                                ddl2.ClearSelection();
                                //int iue = ddl2.Items.Count;
                                //ddl2.Items.FindByText(dt.Rows[i]["Type"].ToString()).Selected = true;

                                //// Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
                                //pnl_warning.Visible = true;
                                //btnSubmitTimeSheet.Visible = true;

                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "tab1";/////////////////////////
            DropDownList ddl = (DropDownList)sender;
            if (ddl.SelectedItem.Text != "Select")
            {
                ProductCategory c = db.ProductCategories.Where(l => l.name == ddl.SelectedItem.Text).FirstOrDefault();
                Category = c.CatId;
            }


            GridViewRow row = (GridViewRow)ddl.NamingContainer;


            DropDownList ddl2 = (DropDownList)gvProductsNew.Rows[row.RowIndex].Cells[3].FindControl("ddlType");
            //Fill the DropDownList with Data   

            FillDropDownListType(ddl2);

        }

        protected void lbDel_Click(object sender, EventArgs e)
        {
            TabName.Value = "tab1";
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                gvProductsNew.DataSource = dt;
                gvProductsNew.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }
        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }
        protected void gvProductsNew_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("lbDel");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void lbAddNewRow_Click(object sender, EventArgs e)
        {
            TabName.Value = "tab1";
            AddNewRowToGrid();
        }
        public void getAllbeforeSubmit()
        {
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    if (pnl_err.Visible != true)
                    {
                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                        //add new row to DataTable   
                        dtCurrentTable.Rows.Add(drCurrentRow);
                    }
                    //Store the current data to ViewState for future reference   

                    ViewState["CurrentTable"] = dtCurrentTable;


                    for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                    {

                        //extract the TextBox values   

                        TextBox box1 = (TextBox)gvProductsNew.Rows[i].Cells[1].FindControl("txtName");
                        TextBox box2 = (TextBox)gvProductsNew.Rows[i].Cells[4].FindControl("txtDescription");
                        TextBox box3 = (TextBox)gvProductsNew.Rows[i].Cells[5].FindControl("txtPrice");
                        TextBox box4 = (TextBox)gvProductsNew.Rows[i].Cells[6].FindControl("txtQty");

                        dtCurrentTable.Rows[i]["Name"] = box1.Text;
                        dtCurrentTable.Rows[i]["Description"] = box2.Text;
                        dtCurrentTable.Rows[i]["Price"] = box3.Text;
                        dtCurrentTable.Rows[i]["csBoxQty"] = box4.Text;

                        //extract the DropDownList Selected Items   

                        DropDownList ddl1 = (DropDownList)gvProductsNew.Rows[i].Cells[2].FindControl("ddlCategory");
                        DropDownList ddl2 = (DropDownList)gvProductsNew.Rows[i].Cells[3].FindControl("ddlType");

                        // Update the DataRow with the DDL Selected Items   
                        dtCurrentTable.Rows[i]["Category"] = ddl1.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Type"] = ddl2.SelectedItem.Text;
                    }

                    //Rebind the Grid with the current data to reflect changes   
                    //gvInvoice.DataSource = dtCurrentTable;
                    //gvInvoice.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
        }
        private bool BlankCategoryType()
        {
            int ct = 0;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {

                    string prodCategory = (string)dt.Rows[i]["Category"];
                    string prodType = (string)dt.Rows[i]["Type"];

                    if (prodCategory == "Select" || prodCategory == "" || prodType == "Select" || prodType == "")
                    {
                        ct += 1;
                    }



                }
            }

            if (ct > 0)
                return true;
            else
                return false;

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            getAllbeforeSubmit();
            //Save Data
            try
            {
                int rowIndex = 0;
                int flag = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 1; i < dtCurrentTable.Rows.Count; i++)
                        {
                            Product t = new Product();
                            //extract the TextBox and drop values  

                            DropDownList ddl1 = (DropDownList)gvProductsNew.Rows[rowIndex].Cells[2].FindControl("ddlCategory");
                            DropDownList ddl2 = (DropDownList)gvProductsNew.Rows[rowIndex].Cells[3].FindControl("ddlType");
                            TextBox box1 = (TextBox)gvProductsNew.Rows[rowIndex].Cells[1].FindControl("txtName");
                            TextBox box2 = (TextBox)gvProductsNew.Rows[rowIndex].Cells[4].FindControl("txtDescription");
                            TextBox box3 = (TextBox)gvProductsNew.Rows[rowIndex].Cells[5].FindControl("txtPrice");
                            TextBox box4 = (TextBox)gvProductsNew.Rows[rowIndex].Cells[6].FindControl("txtQty");

                            if (!BlankCategoryType())
                            {
                                if (ddl1.SelectedIndex != 0 && box3.Text != "" && box4.Text != "")
                                {
                                    t.CatId = Int32.Parse(ddl1.SelectedValue);
                                    t.TypeId = Int32.Parse(ddl2.SelectedValue);
                                    t.name = box1.Text;
                                    if (box2.Text == null)
                                        t.description = "";
                                    else
                                        t.description = box2.Text;

                                    if (box3.Text == null)
                                        t.price = null;
                                    else
                                        t.price = Int32.Parse(box3.Text);
                                    t.csQty = Int32.Parse(box4.Text);
                                    t.IsDeactive = false;

                                    db.Products.Add(t);

                                    rowIndex++;
                                }
                                else
                                    flag = 1;
                            }
                            else
                                flag = 1;


                        }

                    }
                    db.SaveChanges();
                }
                if (flag == 1)
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Blank!!!')</SCRIPT>");
                else
                {
                    //sendMail("","","","","","","");
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Success')</SCRIPT>");
                    SetInitialRow();
                    pnl_err.Visible = false;
                    LabErr.Text = "";
                    getProductGrid(CategoryIDView, TypeIDView);
                    //Response.Redirect("/WebForm1.aspx");
                }

            }
            catch (Exception ex)
            {

                if (ex.InnerException.InnerException.Message.Contains("duplicate"))
                {
                    pnl_err.Visible = true;
                    LabErr.Text = ex.InnerException.InnerException.Message;
                }
            }
        }
        //////         new tab code end  /////////////////

        protected void ddlProductCategoryView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewCategory();
            getProductGrid(CategoryIDView, TypeIDView);

        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TabName.Value = "tab0";
            gvProducts.PageIndex = e.NewPageIndex;
            filterGridSync();
        }

        protected void DropDownList3_SelectedIndexChanged1(object sender, EventArgs e)
        {
            TabName.Value = "tab0";/////////////////////////
            DropDownList ddl = (DropDownList)sender;
            if (ddl.SelectedItem.Text != "Select")
            {
                ProductCategory c = db.ProductCategories.Where(l => l.name == ddl.SelectedItem.Text).FirstOrDefault();
                Category = c.CatId;
            }


            GridViewRow row = (GridViewRow)ddl.NamingContainer;


            DropDownList ddl2 = (DropDownList)gvProducts.Rows[row.RowIndex].Cells[3].FindControl("DropDownList4");
            //Fill the DropDownList with Data   

            FillDropDownListType(ddl2);
        }

        /// dynamic serach////
        /// 

        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable GetRecords()

        {
            var s = db.Products.Select(m => new
            {
                id = m.ProdId,
                CategoryName = m.ProductCategory.name,
                TypeName = m.ProductType.name,
                ProductName = m.name,
                Description = m.description,
                Price = m.price,
                SingleUnitPrice = (m.price / m.csUnitQty),
                Gst = m.gst,
                Qty = m.csQty,// box qty
                Unit = m.unit,
                csUnitQty = m.csUnitQty, // in box no of Item
                UnitQty = m.unitQty,  // broken box no of item // Single Qty //lose item qty
                Margin = m.sellingMargin,
                gst = m.gst,
                PriceAll = ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) + ((m.price / m.csUnitQty) + (m.price / m.csUnitQty) * m.sellingMargin / 100) * m.gst / 100,
                ProductDeactive = m.IsDeactive,
                mrp=m.mrp
            })
                 .OrderBy(k => k.CategoryName).ToList();

            DataTable dt = LINQResultToDataTable(s);

            return dt;
        }
        private void BindGrid()
        {
            DataTable dt = GetRecords();
            if (dt.Rows.Count > 0)
            {
                gvProducts.DataSource = dt;
                gvProducts.DataBind();
            }
        }

        private void SearchText()
        {
            DataTable dt = GetRecords();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvProducts.SortExpression, txtSearch.Text);
            }

            if (SearchExpression != null)
            {
                dv.RowFilter = "ProductName like" + SearchExpression;
                gvProducts.DataSource = dv;
                gvProducts.DataBind();
            }
        }

        public string Highlight(string InputTxt)
        {
            string Search_Str = txtSearch.Text.ToString();
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(),
            RegexOptions.IgnoreCase);

            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt,
            new MatchEvaluator(ReplaceKeyWords));

            // Set the RegExp to null.
            RegExp = null;
        }

        public string ReplaceKeyWords(Match m)
        {
            return "<span class='highlight'>" + m.Value + "</span>";

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchText();
            ddlProductCategoryView.SelectedIndex = -1;
        }


        /////// product filter by Type  ////
        protected void ddlProductTypeView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewType();
            getProductGrid(CategoryIDView, TypeIDView);
        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (Session["user"].ToString() == "sa")
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[8].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    e.Row.Cells[11].Visible = false;
                    e.Row.Cells[12].Visible = false;
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[8].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    e.Row.Cells[11].Visible = false;
                    e.Row.Cells[12].Visible = false;
                }

            }
        }
    }
}