﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Items.aspx.cs" Inherits="QuickStoreBilling.Admin.Items" %>

<%@ Register Assembly="Microsoft.AspNet.EntityDataSource" Namespace="Microsoft.AspNet.EntityDataSource" TagPrefix="ef" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <style>
        .inner-datepicker {
            display: inline-block;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "tab0";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>

    <asp:HiddenField ID="TabName" runat="server" />

    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Products </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">Products</span></a> </li>
            <%--<li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>--%>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane p-20 active" id="tab0" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvProdUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label runat="server" ID="labCat" Text="" Visible="false" />
                            <asp:Panel ID="pnl_warning" runat="server" Visible="false">
                                <div class="alert alert-warning" role="alert" style="width: 100%">
                                    <asp:Label ID="labWarning" runat="server" Text=""></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnl_err" runat="server" Visible="false">
                                <div class="alert alert-danger" role="alert" style="width: 100%">
                                    <asp:Label ID="LabErr" runat="server" Text=""></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:GridView ID="gvItems" runat="server" ShowFooter="true"
                                AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowHeader="true" OnRowCreated="gvItems_RowCreated">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                    <%--<asp:BoundField DataField="RowNumber" HeaderText="SL" />--%>
                                    <asp:TemplateField HeaderText="Category">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlCategory" runat="server" SelectedValue='<%# Eval("EditItemCategory") %>' AutoPostBack="true"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="validateDdlCategory" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required" ControlToValidate="ddlCategory" runat="server"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                       
                                        <FooterTemplate>
                                            <asp:ImageButton ID="btnAddProduct" runat="server" ImageUrl="assets/images/add.png" Height="25px" OnClick="btnAddProduct_Click" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                         <ItemTemplate>
                                            <asp:Label ID="labType" runat="server" Text='<%# Eval("TypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlType" runat="server" SelectedValue='<%# Eval("EditItemType") %>' AutoPostBack="true"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="validateDdlType" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required" ControlToValidate="ddlType" runat="server"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Width="120px" Text='<%# Eval("ProductName") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server" Width="120px" Text='<%# Eval("Description") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPrice" runat="server" Width="30px" Text='<%# Eval("Price") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQty" runat="server" Text='<%# Eval("Qty") %>'></asp:TextBox>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvItems" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
