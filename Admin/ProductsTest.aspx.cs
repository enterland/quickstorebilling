﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickStoreBilling.Model;
using EntityFramework.Extensions;
using System.Reflection;

namespace QuickStoreBilling.Admin
{
    public partial class ProductsTest : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProductGrid();

            }
        }

        private void getProductGrid()
        {


            var s = db.Products.Select(m => new
            {
                id = m.ProdId,
                CategoryName = m.ProductCategory.name,
                TypeName = m.ProductType.name,
                ProductName = m.name,
                Description = m.description,
                Price = m.price,
                Qty = m.qty
            })
                .OrderBy(k => k.ProductName).ToList();

            gvProducts.DataSource = s;
            gvProducts.DataBind();

            DataTable dt = LINQResultToDataTable(s);
            ViewState["CurrentTable"] = dt;

        }


        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }
        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {



            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProducts.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }


        protected void gvProducts_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvProducts.EditIndex = e.NewEditIndex;
            //gvH2hMaster.Width = Unit.Percentage(100);
            //PnchtId = Int32.Parse(Session["pc"].ToString());
            getProductGrid();
            GridViewRow row = gvProducts.Rows[e.NewEditIndex];
            //(row.Cells[2].Controls[0] as DropDownList).Width = Unit.Pixel(100);
            //(row.Cells[3].Controls[0] as DropDownList).Width = Unit.Pixel(100);
            //(row.Cells[4].Controls[0] as TextBox).Width = Unit.Pixel(20);
            //(row.Cells[5].Controls[0] as TextBox).Width = Unit.Pixel(20);
            //(row.Cells[6].Controls[0] as TextBox).Width = Unit.Pixel(100);
            //(row.Cells[7].Controls[0] as TextBox).Width = Unit.Pixel(100);
            getProductGrid();
        }

        protected void gvProducts_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvProducts.EditIndex = -1;
            getProductGrid();
        }

        protected void gvProducts_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvProducts.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);
            string CatName = ((DropDownList)row.FindControl("DropDownList3")).SelectedValue;
            ProductCategory cat = db.ProductCategories.Single(l => l.name == CatName);

            string typeName = ((DropDownList)row.FindControl("DropDownList4")).SelectedValue;
            ProductType t = db.ProductTypes.Single(l => l.name == typeName);
            string Name = (row.Cells[4].Controls[0] as TextBox).Text;
            string Description = (row.Cells[5].Controls[0] as TextBox).Text;
            string Price = (row.Cells[6].Controls[0] as TextBox).Text;
            string Qty = (row.Cells[7].Controls[0] as TextBox).Text;

            Product m = db.Products.Single(s => s.ProdId == id);

            m.CatId = cat.CatId;
            m.TypeId = t.ProductTypeId;
            m.name = Name.ToUpper();
            m.description = Description;
            m.price = Int32.Parse(Price);
            m.qty = Int32.Parse(Qty);

            db.SaveChanges();


            gvProducts.EditIndex = -1;
            getProductGrid();
        }

        protected void gvProducts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);
            db.Products.Where(s => s.ProdId == id).Delete();
            getProductGrid();
        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                ImageButton ib = (ImageButton)e.Row.FindControl("btnAddProduct");
                if (ib != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            ib.Visible = false;
                        }
                    }
                    else
                    {
                        ib.Visible = false;
                    }
                }
            }
        }

        protected void btnAddProduct_Click(object sender, ImageClickEventArgs e)
        {
            AddNewRowToGrid();
        }

        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            //drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            //for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            //{

                            //    //extract the TextBox values   

                            //    Label box1 = (Label)gvProducts.Rows[i].Cells[3].Controls[0] as Label;//(row.Cells[4].Controls[0] as TextBox).Text
                            //    TextBox box2 = (TextBox)gvProducts.Rows[i].Cells[4].Controls[0] as TextBox;
                            //    TextBox box3 = (TextBox)gvProducts.Rows[i].Cells[5].Controls[0] as TextBox;
                            //    TextBox box4 = (TextBox)gvProducts.Rows[i].Cells[6].Controls[0] as TextBox;



                            //    dtCurrentTable.Rows[i]["ProductName"] = box1.Text;
                            //    dtCurrentTable.Rows[i]["Description"] = box2.Text;
                            //    dtCurrentTable.Rows[i]["Price"] = box3.Text;
                            //    dtCurrentTable.Rows[i]["Qty"] = box4.Text;






                            //    //extract the DropDownList Selected Items   

                            //    DropDownList ddl1 = (DropDownList)gvProducts.Rows[i].Cells[1].FindControl("DropDownList3");

                            //    // Update the DataRow with the DDL Selected Items   

                            //    dtCurrentTable.Rows[i]["Category"] = ddl1.SelectedItem.Text;

                            //    //job = ddl1.SelectedItem.Text;
                            //    //NonEditedFieldLeaveDay(job, box2, box3, box4);


                            //    DropDownList ddl2 = (DropDownList)gvProducts.Rows[i].Cells[2].FindControl("DropDownList4");
                            //    dtCurrentTable.Rows[i]["Type"] = ddl2.SelectedItem.Text;



                            //}

                            //Rebind the Grid with the current data to reflect changes   
                            gvProducts.DataSource = dtCurrentTable;
                            gvProducts.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                   // getProductGrid();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        //ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                gvProducts.DataSource = dt;
                gvProducts.DataBind();
            }

            //Set Previous Data on Postbacks  
            getProductGrid();
        }
        //private void SetPreviousData()
        //{

        //    try
        //    {
        //        int rowIndex = 0;
        //        if (ViewState["CurrentTable"] != null)
        //        {

        //            DataTable dt = (DataTable)ViewState["CurrentTable"];
        //            if (dt.Rows.Count > 0)
        //            {

        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    TextBox box1 = (TextBox)GridView1.Rows[i].Cells[1].FindControl("txtDate");
        //                    TextBox box2 = (TextBox)GridView1.Rows[i].Cells[4].FindControl("txtIn");
        //                    TextBox box3 = (TextBox)GridView1.Rows[i].Cells[5].FindControl("txtOut");
        //                    TextBox box4 = (TextBox)GridView1.Rows[i].Cells[6].FindControl("txtHour");
        //                    TextBox box5 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("txtDesc");


        //                    DropDownList ddl1 = (DropDownList)GridView1.Rows[rowIndex].Cells[2].FindControl("DropDownList1");
        //                    DropDownList ddl2 = (DropDownList)GridView1.Rows[rowIndex].Cells[3].FindControl("DropDownList2");

        //                    //Fill the DropDownList with Data   
        //                    FillDropDownList(ddl1);

        //                    int iu = ddl1.Items.Count;
        //                    //FillDropDownList2(ddl2);

        //                    if (i < dt.Rows.Count - 1)
        //                    {

        //                        //Assign the value from DataTable to the TextBox   
        //                        box1.Text = dt.Rows[i]["Date"].ToString();
        //                        box2.Text = dt.Rows[i]["In Time"].ToString();
        //                        box3.Text = dt.Rows[i]["Out Time"].ToString();
        //                        box4.Text = dt.Rows[i]["Hours"].ToString();
        //                        box5.Text = dt.Rows[i]["Description"].ToString();


        //                        //Set the Previous Selected Items on Each DropDownList  on Postbacks   
        //                        ddl1.ClearSelection();
        //                        ddl1.Items.FindByText(dt.Rows[i]["Project"].ToString()).Selected = true;
        //                        job = ddl1.SelectedValue;
        //                        NonEditedFieldLeaveDay(job, box2, box3, box4);
        //                        FillDropDownList2(ddl2);


        //                        ddl2.ClearSelection();
        //                        int iue = ddl2.Items.Count;
        //                        ddl2.Items.FindByText(dt.Rows[i]["Activity"].ToString()).Selected = true;

        //                        // Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
        //                        pnl_warning.Visible = true;
        //                        btnSubmitTimeSheet.Visible = true;
        //                        CalMinHour(Convert.ToDateTime(box1.Text));
        //                    }

        //                    rowIndex++;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
        //    }
        //}

    }
}