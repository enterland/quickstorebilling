﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickStoreBilling.Model;
using EntityFramework.Extensions;

namespace QuickStoreBilling.Admin
{
    public partial class Ind : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProductGrid();
            }
        }

        private void getProductGrid()
        {


            var s = db.Products.Select(m => new
            {
                id = m.ProdId,
                CategoryName = m.ProductCategory.name,
                TypeName = m.ProductType.name,
                ProductName = m.name,
                Description = m.description,
                Price = m.price,
                Qty = m.qty
            })
                .OrderBy(k => k.ProductName).ToList();

            gvProducts.DataSource = s;
            gvProducts.DataBind();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProducts.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvProducts_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvProducts.EditIndex = e.NewEditIndex;
            //gvH2hMaster.Width = Unit.Percentage(100);
            //PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getProductGrid();
            GridViewRow row = gvProducts.Rows[e.NewEditIndex];
            //(row.Cells[2].Controls[0] as DropDownList).Width = Unit.Pixel(100);
            //(row.Cells[3].Controls[0] as DropDownList).Width = Unit.Pixel(100);
            //(row.Cells[4].Controls[0] as TextBox).Width = Unit.Pixel(20);
            //(row.Cells[5].Controls[0] as TextBox).Width = Unit.Pixel(20);
            //(row.Cells[6].Controls[0] as TextBox).Width = Unit.Pixel(100);
            //(row.Cells[7].Controls[0] as TextBox).Width = Unit.Pixel(100);
        }

        protected void gvProducts_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvProducts.EditIndex = -1;
            this.getProductGrid();
        }

        protected void gvProducts_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvProducts.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);
            //string CatName = ((DropDownList)row.FindControl("DropDownList3")).SelectedValue;
            //ProductCategory cat = db.ProductCategories.Single(l => l.name == CatName);
            //string typeName = ((DropDownList)row.FindControl("DropDownList4")).SelectedValue;
            //ProductType t = db.ProductTypes.Single(l => l.name == typeName);
            string Name = (row.Cells[2].Controls[0] as TextBox).Text;
            string Description = (row.Cells[3].Controls[0] as TextBox).Text;
            string Price = (row.Cells[4].Controls[0] as TextBox).Text;
            string Qty = (row.Cells[5].Controls[0] as TextBox).Text;

            Product m = db.Products.Single(s => s.ProdId == id);

            //m.CatId = cat.CatId;
            //m.TypeId = t.ProductTypeId;
            m.name = Name.ToUpper();
            m.description = Description;
            m.price = Int32.Parse(Price);
            m.qty = Int32.Parse(Qty);

            db.SaveChanges();


            gvProducts.EditIndex = -1;
            this.getProductGrid();
        }

        protected void gvProducts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvProducts.DataKeys[e.RowIndex].Values[0]);
            db.Products.Where(s => s.ProdId == id).Delete();
            this.getProductGrid();
        }
    }
}