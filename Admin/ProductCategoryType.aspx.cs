﻿using EntityFramework.Extensions;
using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling.Admin
{
    public partial class ProductCategoryType : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "ad")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
            if (!IsPostBack)
            {
                getProductCategoryGrid();
                getDdlCategoryData();
                getProductTypeGrid();
            }
        }
        private void getProductCategoryGrid()
        {
            var s = db.ProductCategories.Select(m => new
            {
                id = m.CatId,
                ProductCategoryName = m.name
            }).OrderBy(k => k.ProductCategoryName).ToList();

            gvProductCategory.DataSource = s;
            gvProductCategory.DataBind();
        }
        protected void gvProductCategory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProductCategory.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }


        protected void gvProductCategory_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TabName.Value = "tab0";
            gvProductCategory.EditIndex = e.NewEditIndex;

            getProductCategoryGrid();
            GridViewRow row = gvProductCategory.Rows[e.NewEditIndex];

            getProductCategoryGrid();
            getDdlCategoryData();
            
        }

        protected void gvProductCategory_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TabName.Value = "tab0";
            gvProductCategory.EditIndex = -1;
            getProductCategoryGrid();
            getDdlCategoryData();
                    }

        protected void gvProductCategory_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TabName.Value = "tab0";
            GridViewRow row = gvProductCategory.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvProductCategory.DataKeys[e.RowIndex].Values[0]);

            string Name = (row.Cells[2].Controls[0] as TextBox).Text;


            ProductCategory m = db.ProductCategories.Single(s => s.CatId == id);

            m.name = Name;

            db.SaveChanges();


            gvProductCategory.EditIndex = -1;
            getProductCategoryGrid();
            getDdlCategoryData();
            }

        protected void gvProductCategory_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TabName.Value = "tab0";
            int id = Convert.ToInt32(gvProductCategory.DataKeys[e.RowIndex].Values[0]);
            db.ProductCategories.Where(s => s.CatId == id).Delete();
            getProductCategoryGrid();
            getDdlCategoryData();
            }

        protected void btnProdCateSubmit_Click(object sender, EventArgs e)
        {
            TabName.Value = "tab0";
            ProductCategory pc = new ProductCategory
            {
                name = txtProductCategoryName.Text
            };
            db.ProductCategories.Add(pc);
            db.SaveChanges();

            this.getProductCategoryGrid();
            getDdlCategoryData();
           
            txtProductCategoryName.Text = "";


        }

        //----------################################### Product Type ###################################----------

        private void getDdlCategoryData()
        {
            var a = db.ProductCategories.ToList().OrderBy(m => m.name);

            ddlProdCategory.DataSource = a;
            ddlProdCategory.DataValueField = "CatId";
            ddlProdCategory.DataTextField = "name";
            ddlProdCategory.DataBind();
            ddlProdCategory.Items.Insert(0, "Select Category / Company");
        }
        private void getProductTypeGrid()
        {
            var s = db.ProductTypes.Select(m => new
            {
                id = m.ProductTypeId,
                CategoryName = m.ProductCategory.name,
                ProductTypeName = m.name
            }).OrderBy(k => k.ProductTypeName).ToList();

            gvProductType.DataSource = s;
            gvProductType.DataBind();
        }
        protected void gvProductType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            TabName.Value = "tab1";
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProductType.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }


        protected void gvProductType_RowEditing(object sender, GridViewEditEventArgs e)
        {
            TabName.Value = "tab1";
            gvProductType.EditIndex = e.NewEditIndex;

            getProductTypeGrid();
            GridViewRow row = gvProductType.Rows[e.NewEditIndex];

            getProductTypeGrid();
            
        }

        protected void gvProductType_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            TabName.Value = "tab1";
            gvProductType.EditIndex = -1;
            getProductTypeGrid();
            
        }

        protected void gvProductType_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            TabName.Value = "tab1";
            GridViewRow row = gvProductType.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvProductType.DataKeys[e.RowIndex].Values[0]);
            string prodCategoryName = ((DropDownList)row.FindControl("ddlProdCategory")).SelectedValue;
            ProductCategory cat = db.ProductCategories.Single(l => l.name == prodCategoryName);
            string Name = (row.Cells[3].Controls[0] as TextBox).Text;


            ProductType m = db.ProductTypes.Single(s => s.ProductTypeId == id);

            m.ProductCatId = cat.CatId;
            m.name = Name;

            db.SaveChanges();


            gvProductType.EditIndex = -1;
            getProductTypeGrid();
        }

        protected void gvProductType_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TabName.Value = "tab1";
            int id = Convert.ToInt32(gvProductType.DataKeys[e.RowIndex].Values[0]);
            db.ProductTypes.Where(s => s.ProductTypeId == id).Delete();
            getProductTypeGrid();
           }

        protected void btnProdTypeSubmit_Click(object sender, EventArgs e)
        {
            TabName.Value = "tab1";
            ProductType ty = new ProductType
            {
                ProductCatId = Int32.Parse(ddlProdCategory.SelectedValue),
                name = txtProdType.Text

            };
            db.ProductTypes.Add(ty);
            db.SaveChanges();

            txtProductCategoryName.Text = "";

            getProductTypeGrid();
        }
    }
}