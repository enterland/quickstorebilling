﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TimeSheet5.JobCardWS;
using System.Net;
using TimeSheet5.TimesheetEntryWS;
using TimeSheet5.CUTimeSheetLeaveWS;
using TimeSheet5.EmployeeCardWS;
using System.Web.Services;
using TimeSheet5.JobTaskLineWS;
using TimeSheet5.MonthlyWS;
using System.Net.Mail;
using System.IO;
using System.Text;



namespace TimeSheet5
{
    public partial class TimeSheet : System.Web.UI.Page
    {
        public string uNo = "";
        public string manager = "";
        public string job = "";
        string status = "";
        string project = "";
        string month = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                uNo = Session["uNo"].ToString();
                if (Session["ut"].ToString() == "CLMAN")
                    Response.Redirect("/EmpTimeSheetApprove.aspx");
                SetInitialRow();
                getddlTimeSheetStatus();
                getDdlProjets();
                getDdlMonth();
                getGvTimeSheetHistoryManager(status, project, month);
                //timeSelect();

            }
        }



        public void getddlTimeSheetStatus()
        {
            ddlTimeSheetStatus.Items.Insert(0, "Select");
            ddlTimeSheetStatus.Items.Insert(1, "All");
            ddlTimeSheetStatus.Items.Insert(2, TimeSheet5.TimesheetEntryWS.Status.Pending_For_Approval.ToString());
            ddlTimeSheetStatus.Items.Insert(3, TimeSheet5.TimesheetEntryWS.Status.Rejected.ToString());
            ddlTimeSheetStatus.Items.Insert(4, TimeSheet5.TimesheetEntryWS.Status.Approved.ToString());
        }

        public void getDdlProjets()                                     // Projects OR Jobs
        {

            JobCard_Service jobServices = new JobCard_Service();
            jobServices.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            List<JobCard_Filter> filterArray = new List<JobCard_Filter>();
            JobCard_Filter filter1 = new JobCard_Filter();
            filter1.Field = JobCard_Fields.No;
            filter1.Criteria = "*";
            filterArray.Add(filter1);

            JobCard[] jc = jobServices.ReadMultiple(filterArray.ToArray(), null, 100);

            //ddlProject.DataSource = jc;
            //ddlProject.DataTextField = "No";
            //ddlProject.DataValueField = "No";
            //ddlProject.DataBind();



            ddlProject.Items.Clear();
            foreach (JobCard jo in jc)
            {
                ddlProject.Items.Add(jo.No + "-" + jo.Description);
            }
            ddlProject.Items.Insert(0, "Select");
            ddlProject.Items.Insert(1, "All");
        }

        public void getDdlMonth()
        {

            Monthly_Service montSrv = new Monthly_Service();
            montSrv.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            List<Monthly_Filter> m = new List<Monthly_Filter>();

            Monthly[] month = montSrv.ReadMultiple(m.ToArray(), null, 100);

            ddlMonth.Items.Clear();
            foreach (Monthly mm in month)
            {
                ddlMonth.Items.Add(mm.Period_Start.ToString("MMM") + "-" + mm.Period_Start.ToShortDateString() + ".." + mm.Period_End.ToShortDateString());
            }

            ddlMonth.Items.Insert(0, "Select");
            ddlMonth.Items.Insert(1, "All");
        }
        private void FillDropDownList2(DropDownList ddl2)                           // Activity OR JobTaskLine
        {
            JobTaskLine_Service jobTaskService = new JobTaskLine_Service();
            jobTaskService.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            List<JobTaskLine_Filter> JobTaskArray = new List<JobTaskLine_Filter>();
            ddl2.Items.Clear();
            if (job != "")
            {
                JobTaskLine_Filter f1 = new JobTaskLine_Filter();
                f1.Field = JobTaskLine_Fields.Job_No;
                f1.Criteria = getProjectNoDes(job)[0];
                JobTaskArray.Add(f1);


                JobTaskLine[] jb = jobTaskService.ReadMultiple(JobTaskArray.ToArray(), null, 100);


                ddl2.DataSource = jb;
                ddl2.DataTextField = "Description";
                ddl2.DataValueField = "Job_Task_No";
                ddl2.DataBind();
                //foreach (JobTaskLine j in jb)
                //{
                //    ddl2.Items.Add(new ListItem(j.Description, j.Job_Task_No));
                //    //ddl2.Items.Add(new ListItem(ec.First_Name + " " + ec.Last_Name, ec.No));
                //}

                ddl2.Items.Insert(0, "Select");

            }






        }
        private void FillDropDownList(DropDownList ddl)                                     // Projects OR Jobs
        {

            JobCard_Service jobServices = new JobCard_Service();
            jobServices.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            List<JobCard_Filter> filterArray = new List<JobCard_Filter>();
            JobCard_Filter filter1 = new JobCard_Filter();
            filter1.Field = JobCard_Fields.No;
            filter1.Criteria = "*";
            filterArray.Add(filter1);

            JobCard[] jc = jobServices.ReadMultiple(filterArray.ToArray(), null, 100);


            //ddl.DataSource = jc;
            //ddl.DataTextField = "Description";
            //ddl.DataValueField = "No";
            //ddl.DataBind();

            ddl.Items.Clear();
            foreach (JobCard jo in jc)
            {
                ddl.Items.Add(jo.No + "-" + jo.Description);
            }

            ddl.Items.Insert(0, "Select");

        }
        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Project", typeof(string)));
            dt.Columns.Add(new DataColumn("Activity", typeof(string)));
            dt.Columns.Add(new DataColumn("In Time", typeof(string)));
            dt.Columns.Add(new DataColumn("Out Time", typeof(string)));
            dt.Columns.Add(new DataColumn("Hours", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Date"] = string.Empty;
            dr["In Time"] = string.Empty;
            dr["Out Time"] = string.Empty;
            dr["Hours"] = string.Empty;
            dr["Description"] = string.Empty;

            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            GridView1.DataSource = dt;
            GridView1.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data   
            DropDownList ddl1 = (DropDownList)GridView1.Rows[0].Cells[2].FindControl("DropDownList1");

            FillDropDownList(ddl1);



        }
        

        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                                //extract the TextBox values   

                                TextBox box1 = (TextBox)GridView1.Rows[i].Cells[1].FindControl("txtDate");
                                TextBox box2 = (TextBox)GridView1.Rows[i].Cells[4].FindControl("txtIn");
                                TextBox box3 = (TextBox)GridView1.Rows[i].Cells[5].FindControl("txtOut");
                                TextBox box4 = (TextBox)GridView1.Rows[i].Cells[6].FindControl("txtHour");
                                TextBox box5 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("txtDesc");


                                dtCurrentTable.Rows[i]["Date"] = box1.Text;
                                dtCurrentTable.Rows[i]["In Time"] = box2.Text;
                                dtCurrentTable.Rows[i]["Out Time"] = box3.Text;
                                dtCurrentTable.Rows[i]["Hours"] = box4.Text;
                                dtCurrentTable.Rows[i]["Description"] = box5.Text;





                                //extract the DropDownList Selected Items   

                                DropDownList ddl1 = (DropDownList)GridView1.Rows[i].Cells[2].FindControl("DropDownList1");

                                // Update the DataRow with the DDL Selected Items   

                                dtCurrentTable.Rows[i]["Project"] = ddl1.SelectedItem.Text;

                                job = ddl1.SelectedItem.Text;
                                NonEditedFieldLeaveDay(job, box2, box3, box4);


                                DropDownList ddl2 = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("DropDownList2");
                                dtCurrentTable.Rows[i]["Activity"] = ddl2.SelectedItem.Text;



                            }

                            //Rebind the Grid with the current data to reflect changes   
                            GridView1.DataSource = dtCurrentTable;
                            GridView1.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)GridView1.Rows[i].Cells[1].FindControl("txtDate");
                            TextBox box2 = (TextBox)GridView1.Rows[i].Cells[4].FindControl("txtIn");
                            TextBox box3 = (TextBox)GridView1.Rows[i].Cells[5].FindControl("txtOut");
                            TextBox box4 = (TextBox)GridView1.Rows[i].Cells[6].FindControl("txtHour");
                            TextBox box5 = (TextBox)GridView1.Rows[i].Cells[7].FindControl("txtDesc");


                            DropDownList ddl1 = (DropDownList)GridView1.Rows[rowIndex].Cells[2].FindControl("DropDownList1");
                            DropDownList ddl2 = (DropDownList)GridView1.Rows[rowIndex].Cells[3].FindControl("DropDownList2");

                            //Fill the DropDownList with Data   
                            FillDropDownList(ddl1);

                            int iu = ddl1.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   
                                box1.Text = dt.Rows[i]["Date"].ToString();
                                box2.Text = dt.Rows[i]["In Time"].ToString();
                                box3.Text = dt.Rows[i]["Out Time"].ToString();
                                box4.Text = dt.Rows[i]["Hours"].ToString();
                                box5.Text = dt.Rows[i]["Description"].ToString();


                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                ddl1.ClearSelection();
                                ddl1.Items.FindByText(dt.Rows[i]["Project"].ToString()).Selected = true;
                                job = ddl1.SelectedValue;
                                NonEditedFieldLeaveDay(job, box2, box3, box4);
                                FillDropDownList2(ddl2);


                                ddl2.ClearSelection();
                                int iue = ddl2.Items.Count;
                                ddl2.Items.FindByText(dt.Rows[i]["Activity"].ToString()).Selected = true;

                                // Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
                                pnl_warning.Visible = true;
                                btnSubmitTimeSheet.Visible = true;
                                CalMinHour(Convert.ToDateTime(box1.Text));
                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                LinkButton lb = (LinkButton)e.Row.FindControl("LinkButton1");
                if (lb != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        if (e.Row.RowIndex == dt.Rows.Count - 1)
                        {
                            lb.Visible = false;
                        }
                    }
                    else
                    {
                        lb.Visible = false;
                    }
                }
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 1)
                {
                    if (gvRow.RowIndex < dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        dt.Rows.Remove(dt.Rows[rowID]);
                        ResetRowID(dt);
                    }
                }

                //Store the current data in ViewState for future reference  
                ViewState["CurrentTable"] = dt;

                //Re bind the GridView for the updated data  
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

            //Set Previous Data on Postbacks  
            SetPreviousData();
        }

        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }

        protected void lb2_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void btnSubmitTimeSheet_Click(object sender, EventArgs e)
        {

            TimesheetEntry_Service te_service = new TimesheetEntry_Service();
            te_service.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            //Save Data
            try
            {
                int rowIndex = 0;
                int flag = 0;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    if (dtCurrentTable.Rows.Count > 0)
                    {

                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            TimesheetEntry t = new TimesheetEntry();
                            //extract the TextBox and drop values  

                            TextBox boxDate = (TextBox)GridView1.Rows[rowIndex].Cells[1].FindControl("txtDate");
                            DropDownList ddl1 = (DropDownList)GridView1.Rows[rowIndex].Cells[1].FindControl("DropDownList1");
                            TextBox boxIn = (TextBox)GridView1.Rows[rowIndex].Cells[4].FindControl("txtIn");
                            TextBox boxOut = (TextBox)GridView1.Rows[rowIndex].Cells[5].FindControl("txtOut");
                            TextBox boxHour = (TextBox)GridView1.Rows[rowIndex].Cells[6].FindControl("txtHour");
                            TextBox boxDesc = (TextBox)GridView1.Rows[rowIndex].Cells[7].FindControl("txtDesc");
                            DropDownList ddl2 = (DropDownList)GridView1.Rows[rowIndex].Cells[3].FindControl("DropDownList2");

                            string projectCode = "";
                            if (!BlankProjORactivity())
                            {
                                if (ddl1.SelectedIndex != 0 && ddl2.SelectedIndex != 0 && boxIn.Text != "" && boxOut.Text != "" && boxHour.Text != "")
                                {
                                    CUTimeSheetLeave c = new CUTimeSheetLeave();
                                    c.Credentials = new NetworkCredential("administrator", "U@ss##156#");
                                    t.Day = c.GetDay(Convert.ToDateTime(boxDate.Text));
                                    te_service.Create(ref t);

                                    t.Date = Convert.ToDateTime(boxDate.Text);
                                    te_service.Update(ref t);

                                    projectCode = getProjectNoDes(ddl1.SelectedValue)[0];
                                    t.Project_Name = projectCode;
                                    t.Job_Task_Line = ddl2.SelectedItem.Text;


                                    // If Leave Take Full or Half Day or Holiday or Week Off
                                    if (projectCode == "66" || projectCode == "99" || projectCode == "88" || ddl2.SelectedItem.Text == "Leave - Full Day")
                                    {
                                        t.In_time = Convert.ToDateTime("0:00:00 AM");
                                        t.Out_time = Convert.ToDateTime("0:00:00 AM");
                                        t.Regular_Hours = 0.0m;
                                        t.OverTime = 0.0m;
                                    }
                                    else if (ddl2.SelectedItem.Text == "Leave - 1'st Half")
                                    {
                                        t.In_time = Convert.ToDateTime("9:00:00 AM");
                                        t.Out_time = Convert.ToDateTime("1:00:00 PM");
                                        t.Regular_Hours = 4.0m;
                                        t.OverTime = 0.0m;
                                    }
                                    else if (ddl2.SelectedItem.Text == "Leave - 2'nd Half")
                                    {
                                        t.In_time = Convert.ToDateTime("14:00:00 PM");
                                        t.Out_time = Convert.ToDateTime("18:00:00 PM");
                                        t.Regular_Hours = 4.0m;
                                        t.OverTime = 0.0m;
                                    }
                                    else
                                    {
                                        t.In_time = Convert.ToDateTime(boxIn.Text);
                                        t.Out_time = Convert.ToDateTime(boxOut.Text);
                                        t.Regular_Hours = Convert.ToDecimal(boxHour.Text);
                                        decimal tt = c.GetOT(DateTime.Parse(boxIn.Text), DateTime.Parse(boxOut.Text));
                                        tt = Math.Round(tt, 2);
                                        t.OverTime = tt;
                                    }

                                    te_service.Update(ref t);

                                    t.Task_Description = boxDesc.Text;
                                    t.Status = TimeSheet5.TimesheetEntryWS.Status.Pending_For_Approval;

                                    t.Submit_Date = DateTime.Now.Date;
                                    te_service.Update(ref t);

                                    EmployeeCard_Service es = new EmployeeCard_Service();
                                    es.Credentials = new NetworkCredential("administrator", "U@ss##156#");

                                    EmployeeCard em = es.Read(Session["uNo"].ToString());

                                    t.Resource = em.Resource_No;
                                    te_service.Update(ref t);
                                    t.Reporting_To = em.Manager_No;
                                    te_service.Update(ref t);

                                    rowIndex++;
                                }
                                else
                                    flag = 1;
                            }
                            else
                                flag = 1;
                        }

                    }
                }
                if (flag == 1)
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Blank!!! Please Select Project or Activity. Or check In/Out Time')</SCRIPT>");
                else
                {
                    //sendMail("","","","","","","");
                    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Success')</SCRIPT>");
                    SetInitialRow();
                    getGvTimeSheetHistoryManager(status, project, month);
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private bool BlankProjORactivity()
        {
            int ct = 0;
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {

                    string proj = (string)dt.Rows[i]["Project"];
                    string activity = (string)dt.Rows[i]["Activity"];

                    if (proj == "Select" || proj == "")
                    {
                        ct += 1;
                    }
                    if (activity == "Select" || activity == "")
                    {
                        ct += 1;
                    }


                }
            }

            if (ct > 0)
                return true;
            else
                return false;

        }

        [WebMethod]
        public static string isSunLSatHoliLeave(string rowDate)
        {
            CUTimeSheetLeave c = new CUTimeSheetLeave();
            c.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            DateTime dti = Convert.ToDateTime(rowDate);

            TimeSheet t = new TimeSheet();

            string result = c.IsSunLSatHolidayLeave(dti, t.uNo);

            return result;

        }

        public void getGvTimeSheetHistoryManager(string tStatus, string prj, string month)
        {
            try
            {
                EmployeeCard_Service emService = new EmployeeCard_Service();
                emService.Credentials = new NetworkCredential("administrator", "U@ss##156#");

                JobCard_Service j_Service = new JobCard_Service();
                j_Service.Credentials = new NetworkCredential("administrator", "U@ss##156#");

                //ResourceCard_Service recsService=new ResourceCard_Service();
                //recsService.Credentials=new NetworkCredential("administrator", "U@ss##156#");

                EmployeeCard ee = emService.Read(Session["uNo"].ToString());


                TimesheetEntry_Service timeSheetService = new TimesheetEntry_Service();
                timeSheetService.Credentials = new NetworkCredential("administrator", "U@ss##156#");

                List<TimesheetEntry_Filter> ldFilterArray = new List<TimesheetEntry_Filter>();

                TimesheetEntry_Filter clFill = new TimesheetEntry_Filter();
                clFill.Field = TimesheetEntry_Fields.Resource;
                clFill.Criteria = ee.Resource_No;
                ldFilterArray.Add(clFill);

                if (tStatus != "" && tStatus != "All" && tStatus != "Select")
                {
                    TimesheetEntry_Filter filter2 = new TimesheetEntry_Filter();
                    filter2.Field = TimesheetEntry_Fields.Status;
                    filter2.Criteria = tStatus;
                    ldFilterArray.Add(filter2);
                }

                if (prj != "" && prj != "All" && prj != "Select")
                {
                    TimesheetEntry_Filter filter3 = new TimesheetEntry_Filter();
                    filter3.Field = TimesheetEntry_Fields.Project_Name;
                    filter3.Criteria = prj;
                    ldFilterArray.Add(filter3);
                }
                if (month != "" && month != "All" && month != "Select")
                {
                    TimesheetEntry_Filter filMonth = new TimesheetEntry_Filter();
                    filMonth.Field = TimesheetEntry_Fields.Date;
                    filMonth.Criteria = month;
                    ldFilterArray.Add(filMonth);
                }


                TimesheetEntry[] ld = timeSheetService.ReadMultiple(ldFilterArray.ToArray(), null, 10000);

                DataTable dt = new DataTable();

                //dt.Columns.Add(new DataColumn("First_Name", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Date", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Project", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("InOut", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Hours", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Desc", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("OT", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("reprt", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("resMgn", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Status", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("View", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("EntryNo", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("qString", Type.GetType("System.String")));

                string queryString = "";
                string resource = "";
                string FName = "";
                string LName = "";
                string sts = "";
                string Rejectsts = "";
                foreach (TimesheetEntry l in ld)
                {
                    DataRow dr = dt.NewRow();

                    EmployeeCard r = emService.Read(Session["uNo"].ToString());

                    //dr[0] = r.First_Name;
                    dr[0] = l.Date.ToString("ddd dd-MMM-yy");
                    dr[1] = l.Day;
                    dr[2] = l.Project_Name;
                    dr[3] = Convert.ToDateTime(l.In_time).ToShortTimeString() + " - " + Convert.ToDateTime(l.Out_time).ToShortTimeString();
                    dr[4] = l.Regular_Hours;
                    dr[5] = l.Task_Description;
                    dr[6] = l.OverTime;
                    EmployeeCard em = emService.Read(l.Reporting_To);
                    dr[7] = em.First_Name + " " + em.Last_Name;

                    List<EmployeeCard_Filter> emArray = new List<EmployeeCard_Filter>();
                    EmployeeCard_Filter f1 = new EmployeeCard_Filter();
                    f1.Field = EmployeeCard_Fields.Resource_No;
                    f1.Criteria = l.Resource;
                    emArray.Add(f1);
                    EmployeeCard[] emp = emService.ReadMultiple(emArray.ToArray(), null, 100);
                    string a = "";
                    foreach (EmployeeCard r1 in emp)
                    {
                        a = r1.First_Name + " " + r1.Last_Name;
                        FName = r1.First_Name;
                        LName = r1.Last_Name;
                    }

                    string ResponsablePerson = "";
                    if (l.Responsible_resource != null)
                    {
                        List<EmployeeCard_Filter> emArrayRes = new List<EmployeeCard_Filter>();
                        EmployeeCard_Filter fl1 = new EmployeeCard_Filter();
                        fl1.Field = EmployeeCard_Fields.Resource_No;
                        fl1.Criteria = l.Responsible_resource;
                        emArrayRes.Add(fl1);
                        EmployeeCard[] empResP = emService.ReadMultiple(emArrayRes.ToArray(), null, 100);

                        foreach (EmployeeCard rs in empResP)
                            ResponsablePerson = rs.First_Name + " " + rs.Last_Name;

                        dr[8] = ResponsablePerson;
                    }
                    else
                        dr[8] = "";



                    if (ResponsablePerson == em.First_Name + " " + em.Last_Name)
                        dr[8] = "";
                    else
                        dr[8] = ResponsablePerson;

                    if (l.Status == TimeSheet5.TimesheetEntryWS.Status.Pending_For_Approval && l.Escalation_Level_Riched == 0)
                        sts = "Pending";
                    else if (l.Approved_by_CM && l.Escalation_Level == 2 && l.Escalation_Level_Riched == 1)
                        sts = "Client(✓) - Manager";
                    else if (l.Approved_by_reporting_manager && l.Escalation_Level == 3 && l.Escalation_Level_Riched == 1)
                        sts = "Manager(✓) - Client - Manager ";
                    else if (l.Approved_by_reporting_manager && l.Approved_by_CM && l.Escalation_Level == 3 && l.Escalation_Level_Riched == 2)
                        sts = "Manager(✓) - Client(✓) - Manager";
                    else
                        sts = l.Status.ToString();
                    if (l.Status == TimeSheet5.TimesheetEntryWS.Status.Rejected)
                    {
                        Rejectsts = l.Status + " _By_ " + l.RejectBy;
                        dr[9] = l.Status;
                    }
                    else
                        dr[9] = sts;
                    queryString = "entryNo=" + l.Entry_No + "&eCode=" + l.Resource + "&FName=" + FName + "&LName=" + LName + "&ProjName=" + l.Project_Name + "&sts=" + sts + "&esLv=" + l.Escalation_Level + "&rej=" + Rejectsts;
                    queryString = queryString.Replace(" ", "");


                    //dr[10] = "<a style='font-size: 25px;color: #6dd7e8;' class='m-r-10 mdi mdi-eye'  href=TimesheetView.aspx? " + queryString + "&v=1></a>";

                    //dr[10] = "<a style='font-size: 25px;color: #6dd7e8;' class='m-r-10 mdi mdi-eye' href='#'  onclick=window.open('TimesheetView.aspx?" + queryString + "&v=1'></a>";



                    dr[10] = "";


                    //<a href="#" onclick="window.open('http://www.w3schools.com','_blank','toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=400, height=400');return false">Link text</a>

                    //dr[10] = "";
                    dr[11] = l.Entry_No;
                    dr[12] = queryString;
                    dt.Rows.Add(dr);

                }


                ViewState["CurrentTable1"] = dt;
                gvTimeSheet.DataSource = dt;
                gvTimeSheet.DataBind();

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            job = (string)ddl.SelectedValue;


            GridViewRow row = (GridViewRow)ddl.NamingContainer;


            DropDownList ddl2 = (DropDownList)GridView1.Rows[row.RowIndex].Cells[3].FindControl("DropDownList2");
            TextBox box2 = (TextBox)GridView1.Rows[row.RowIndex].Cells[4].FindControl("txtIn");
            TextBox box3 = (TextBox)GridView1.Rows[row.RowIndex].Cells[5].FindControl("txtOut");
            TextBox box4 = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtHour");


            //Fill the DropDownList with Data   

            FillDropDownList2(ddl2);

            NonEditedFieldLeaveDay(job, box2, box3, box4);


        }

        protected void ddlTimeSheetStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            status = ddlTimeSheetStatus.SelectedValue;

            getGvTimeSheetHistoryManager(status, project, month);
            ddlProject.SelectedIndex = -1;
            ddlMonth.SelectedIndex = -1;
            btnGetReport.Visible = false;
        }

        private string[] getProjectNoDes(string prj)
        {
            string[] nodes = new string[1];
            nodes = prj.Split('-');
            return nodes;
        }

        private void NonEditedFieldLeaveDay(string job, TextBox box2, TextBox box3, TextBox box4)
        {
            string job1 = getProjectNoDes(job)[0];
            if (job1 == "66")
            {
                box2.Enabled = false;
                box3.Enabled = false;
            }
            else
            {
                box2.Enabled = true;
                box3.Enabled = true;
            }
        }
        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            // project = ddlProject.SelectedValue;

            project = getProjectNoDes(ddlProject.SelectedValue)[0];

            getGvTimeSheetHistoryManager(status, project, month);
            ddlTimeSheetStatus.SelectedIndex = -1;
            ddlMonth.SelectedIndex = -1;
        }

        protected void gvTimeSheet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[9].Text == TimeSheet5.TimesheetEntryWS.Status.Approved.ToString())
                    e.Row.Cells[9].CssClass = "text-success";
                else if (e.Row.Cells[9].Text == "Pending" || e.Row.Cells[9].Text == "Client(✓) - Manager" || e.Row.Cells[9].Text == "Manager(✓) - Client - Manager " || e.Row.Cells[9].Text == "Manager(✓) - Client(✓) - Manager")
                    e.Row.Cells[5].Attributes["style"] = "color: #3e469a;";
                else
                    e.Row.Cells[9].CssClass = "text-danger";

            }
        }

        protected void gvTimeSheet_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //int id = Convert.ToInt32(e.CommandArgument);
            string qStr = e.CommandArgument.ToString();

            switch (e.CommandName)
            {
                case "ViewRow":
                    string url = "TimesheetView.aspx?" + qStr;

                    string s = "window.open('" + url + "', 'popup_window', 'width=600,height=800,left=100,top=100,resizable=yes');";

                    ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                    break;
                default:
                    break;
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //month = ddlMonth.SelectedValue;

            month = getProjectNoDes(ddlMonth.SelectedValue)[1];

            getGvTimeSheetHistoryManager(status, project, month);
            ddlTimeSheetStatus.SelectedIndex = -1;
            ddlProject.SelectedIndex = -1;

            if (ddlMonth.SelectedIndex > 1)
                btnGetReport.Visible = true;

        }

        public void sendMail(string from, string to, string bccCEO, string ccRM, string ccEmp, string subject, string body)
        {

            try
            {
                MailMessage msg = new MailMessage();

                msg.From = new MailAddress("mma.maahi@gmail.com"); //from
                msg.To.Add("mma.maahi1@gmail.com");  // to
                msg.Bcc.Add(bccCEO);
                msg.CC.Add(ccRM);
                msg.CC.Add(ccEmp);
                msg.Subject = "test";
                msg.Body = "Test Content";
                //msg.Priority = MailPriority.High;


                using (SmtpClient client = new SmtpClient())
                {
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential("mma.maahi@gmail.com", "maahimz143");
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    client.Send(msg);
                }
            }

            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        protected void getReport_Click(object sender, EventArgs e)
        {
            string mnth = getProjectNoDes(ddlMonth.SelectedValue)[1];
            string url = "/Reporting.aspx?emNo=" + Session["uNo"].ToString() + "&mnth=" + mnth;

            string s = "window.open('" + url + "', 'popup_window', 'width=800,height=800,left=100,top=100,resizable=yes');";

            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
            // Response.Redirect("/Reporting.aspx?emNo=" + Session["uNo"].ToString()+ "&mnth=" + ddlMonth.SelectedValue);
        }

        public string calRegulerHour(string In, string Out, string dtd)
        {
            try
            {
                pnl_err.Visible = false;
                DateTime InTime = DateTime.Parse(In);
                DateTime OutTime = DateTime.Parse(Out);
                DateTime dtt = Convert.ToDateTime(dtd);
                string OutString = OutTimeValidation(InTime, OutTime, dtt);
                if (OutString == "Earlier")
                {
                    pnl_err.Visible = true;
                    LabErr.Text = "Out Time is Earlier than In Time!!!";
                    return "";
                }
                else if (OutString == "Same")
                {
                    pnl_err.Visible = true;
                    LabErr.Text = "Out Time and In Time is Same!!!";
                    return "";
                }
                else
                {
                    TimeSpan ts = OutTime - InTime;
                    return ts.TotalHours.ToString();
                }

            }
            catch (Exception)
            {
                pnl_err.Visible = true;
                LabErr.Text = "Please Check In/Out Time !!!";
                return "";
            }
        }

        private string OutTimeValidation(DateTime InTime, DateTime OutTime, DateTime dtd)
        {
            try
            {
                string InT = dtd.ToShortDateString() + " " + InTime.ToShortTimeString();
                string OutT = dtd.ToShortDateString() + " " + OutTime.ToShortTimeString();
                DateTime In = Convert.ToDateTime(InT);
                DateTime Out = Convert.ToDateTime(OutT);
                int result = DateTime.Compare(Out, In);

                if (result < 0)
                    return "Earlier";
                else if (result == 0)
                    return "Same";
                else
                    return "Later";
            }
            catch (Exception ex)
            {
                return "" + ex;
            }
        }

        protected void txtOut_TextChanged(object sender, EventArgs e)
        {

            TextBox txt = (TextBox)sender;
            GridViewRow row = (GridViewRow)txt.NamingContainer;


            TextBox box1 = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtDate");
            TextBox boxIn = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtIn");
            TextBox boxOut = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtOut");
            TextBox boxHour = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtHour");
            boxHour.Text = calRegulerHour(boxIn.Text, boxOut.Text, box1.Text);

            //pnl_warning.Visible = true;
            //btnSubmitTimeSheet.Visible = true;
            //CalMinHour(Convert.ToDateTime(box1.Text));


        }

        public void CalMinHour(DateTime onDate)
        {
            try
            {
                string un = Session["uNo"].ToString();
                DataSet ds = new DataSet();
                WSproxy p = new WSproxy();

                TimesheetEntry[] ll = p.byDateTimesheetEntry(onDate, onDate);
                ds = timesheetTable(ll);


                DataTable dt = new DataTable();


                dt = (DataTable)ViewState["dtTimesheetEntry"];
                decimal hour = 0.0m;
                decimal postedHour = 0.0m;
                decimal newHour = 0.0m;
                bool isLeaveEntry = false;

                if (dt.Rows.Count > 0)
                {
                    foreach (TimesheetEntry l in ll)
                    {
                        hour += l.Regular_Hours;
                    }
                    postedHour = hour;

                }
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                        {
                            DateTime bxDate = Convert.ToDateTime(dtCurrentTable.Rows[i]["Date"]);
                            if (bxDate == onDate)
                            {
                                string bxhour = dtCurrentTable.Rows[i]["Hours"].ToString();
                                string ddlProj = (string)dtCurrentTable.Rows[i]["Project"];
                                string projActivity = (string)dtCurrentTable.Rows[i]["Activity"];
                                string InT = (string)dtCurrentTable.Rows[i][4];
                                string OutT = (string)dtCurrentTable.Rows[i][5];
                                string job = getProjectNoDes(ddlProj)[0];

                                // Entry Timesheet if project is = Leave
                                if (job == "66")
                                {
                                    isLeaveEntry = true;
                                    entryTimesheetOnLeave(onDate, projActivity, postedHour, hour);
                                }
                                else
                                {
                                    decimal dl = decimal.Parse(bxhour);
                                    hour += dl;
                                }

                            }

                        }

                    }
                }

                newHour = hour - postedHour;

                if (!isLeaveEntry)
                {
                    if (hour < 8)
                    {
                        pnl_warning.Visible = true;
                        //labWarning.Text = "On " + onDate.ToShortDateString() + " : (Posted)" + String.Format("{0:0.##}", postedHour) + " + (New)" + String.Format("{0:0.##}", newHour) + " = " + String.Format("{0:0.##}", hour) + " || Detect - Less Effort";
                        labWarning.Text = "On " + onDate.ToShortDateString() + " : Less than 8 hour !!!";
                    }
                    else if (hour > 24)
                    {
                        pnl_warning.Visible = true;
                        //labWarning.Text = "On " + onDate.ToShortDateString() + " : (Posted)" + String.Format("{0:0.##}", postedHour) + " + (New)" + String.Format("{0:0.##}", newHour) + " = " + String.Format("{0:0.##}", hour) + " || Detect - Max Effort ! not Allowed";
                        labWarning.Text = "On " + onDate.ToShortDateString() + " : More than 24 hour !!!";
                    }
                    else
                    {
                        pnl_warning.Visible = false;
                        btnSubmitTimeSheet.Visible = true;
                    }
                }




            }
            catch (Exception)
            {

            }




        }

        private void entryTimesheetOnLeave(DateTime onDate, string activity, decimal postedHour, decimal CrrntDtHour)
        {
            if (activity == "Full Day" && (CrrntDtHour > 0 || postedHour > 0))
            {
                pnl_warning.Visible = true;
                labWarning.Text = "On " + onDate.ToShortDateString() + " : (Posted)" + String.Format("{0:0.##}", postedHour) + " + (New)" + String.Format("{0:0.##}", CrrntDtHour - postedHour) + " : Not allowed, Leave-Full_Day entry and Project entry on same day!!. Please Check Date or Project";
            }
            else if (activity == "1'st Half")
            {
                string InT = "9:00:00";
                string OutT = "1:00:00 pm";
                OverlapTime(onDate, InT, OutT);
            }
            else if (activity == "2'nd Half")
            {
                string InT = "1:00:00 pm";
                string OutT = "5:00:00 pm";
                OverlapTime(onDate, InT, OutT);
            }
        }

        private void OverlapTime(DateTime onDate, string InT, string OutT)
        {
            pnl_warning.Visible = false;
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["dtTimesheetEntry"];
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                dt.Merge(dtCurrentTable);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count - 2; i++)
                    {
                        DateTime dtInt = Convert.ToDateTime(dt.Rows[i]["InTime"]);
                        DateTime dtOut = Convert.ToDateTime(dt.Rows[i]["OutTime"]);

                        if ((Convert.ToDateTime(InT) >= dtInt && Convert.ToDateTime(InT) > dtOut) || (Convert.ToDateTime(OutT) >= dtInt && Convert.ToDateTime(OutT) >= dtOut))
                        {
                            pnl_warning.Visible = true;
                            labWarning.Text = "Overlap the Time!!";
                        }

                    }
                }
            }
        }


        public DataSet timesheetTable(TimesheetEntry[] ld)
        {
            try
            {
                WSproxy p = new WSproxy();
                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Date", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Project", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Activity", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("InTime", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("OutTime", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("RegularHours", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("OTHours", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("TotalPay", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("EmployeeName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Department", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("FirstLineManager", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Description", Type.GetType("System.String")));


                foreach (TimesheetEntry l in ld)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = l.Date.ToString("ddd");
                    dr[1] = l.Date.ToShortDateString();
                    dr[2] = p.oneJob(l.Project_Name).No + "_" + p.oneJob(l.Project_Name).Description;
                    dr[3] = l.Job_Task_Line;
                    dr[4] = l.In_time.ToShortTimeString();
                    dr[5] = l.Out_time.ToShortTimeString();
                    dr[6] = l.Regular_Hours;
                    dr[7] = l.OverTime;
                    dr[8] = "";

                    string a = "";
                    string Dept = "";

                    foreach (EmployeeCard r1 in p.byResourceEmployeeCard(l.Resource))
                    {
                        a = r1.First_Name + " " + r1.Last_Name;
                        Dept = r1.Skill_Set;
                    }
                    dr[9] = a;
                    dr[10] = Dept;
                    dr[11] = p.oneEmployeeCard(l.Reporting_To).First_Name + " " + p.oneEmployeeCard(l.Reporting_To).Last_Name;
                    dr[12] = l.Task_Description;

                    dt.Rows.Add(dr);
                }

                DataSet dd = new DataSet();
                dd.Tables.Add(dt);
                ViewState["dtTimesheetEntry"] = dt;
                return dd;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected void txtIn_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow row = (GridViewRow)txt.NamingContainer;


            TextBox box1 = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtDate");
            TextBox boxIn = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtIn");
            TextBox boxOut = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtOut");
            TextBox boxHour = (TextBox)GridView1.Rows[row.RowIndex].Cells[6].FindControl("txtHour");
            boxHour.Text = calRegulerHour(boxIn.Text, boxOut.Text, box1.Text);
        }


        private void checkSameDayTimeOverlap(DateTime dtd, DateTime In, DateTime Out)
        {

        }



    }
}