﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TimeSheet.aspx.cs" Inherits="TimeSheet5.TimeSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .inner-datepicker {
            display: inline-block;
        }
    </style>
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Time Sheet</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Project & Timesheet</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Time Sheet</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#timeSheet" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">Time Sheet</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#history" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">History</span></a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="timeSheet" role="tabpanel">
                <div class="p-20">
                    <div class="card">
                        <div class="card-body">
                            <asp:ScriptManager ID="ScriptTimeSheet" runat="server" EnablePageMethods="true">
                            </asp:ScriptManager>
                            <div class="form-group row">
                                <label class="col-md-3 m-t-15">Time Sheet Entry</label>
                                <div class="table-responsive">
                                    <asp:UpdatePanel ID="updTimeSheet" UpdateMode="Always" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnl_warning" runat="server" Visible="false">
                                                <div class="alert alert-warning" role="alert" style="width: 100%">
                                                    <asp:Label ID="labWarning" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnl_err" runat="server" Visible="false">
                                                <div class="alert alert-danger" role="alert" style="width: 100%">
                                                    <asp:Label ID="LabErr" runat="server" Text=""></asp:Label>
                                                </div>
                                            </asp:Panel>
                                            <asp:GridView ID="GridView1" runat="server" ShowFooter="true" CssClass="table-striped table-bordered table"
                                                AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowHeader="true" OnRowCreated="GridView1_RowCreated">
                                                <Columns>
                                                    <asp:BoundField DataField="RowNumber" HeaderText="SL" />
                                                    <asp:TemplateField HeaderText="Date">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtDate" runat="server" name="txtDate" Width="80px"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <FooterStyle />
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lb2" runat="server" Style="padding: 0;" OnClick="lb2_Click" CssClass="m-icon"><i style="font-size: 30px;color: #2eb2da;margin-right: 0;" class="m-r-10 mdi mdi-plus-circle"></i></asp:LinkButton>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Project">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DropDownList1" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="recDdlProj" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required" ControlToValidate="DropDownList1" runat="server"></asp:RequiredFieldValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Activity">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DropDownList2" runat="server"
                                                                AppendDataBoundItems="true" required="true">
                                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="recDdlActi" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required" ControlToValidate="DropDownList2" runat="server"></asp:RequiredFieldValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="In Time">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtIn" runat="server" Width="120px" value="9:00 AM" OnTextChanged="txtIn_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Out Time">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtOut" runat="server" Width="120px" value="5:00 PM" OnTextChanged="txtOut_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Hours">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtHour" runat="server" Width="30px" Enabled="false"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtDesc" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Style="padding: 0;" OnClick="LinkButton1_Click" CssClass="m-icon"><i style="font-size: 30px;color: #da542e;margin-right: 0;" class="m-r-10 mdi mdi-minus-circle"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCreated" />
                                            <%--<asp:AsyncPostBackTrigger ControlID="txtOut" EventName="TextChanged" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <br />
                            <div class="border-top">
                                <div class="card-body" style="text-align: right;">
                                    <asp:Button class="btn btn-primary" ID="btnSubmitTimeSheet" runat="server" OnClick="btnSubmitTimeSheet_Click" Text="Submit" />
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <div class="tab-pane  p-20" id="history" role="tabpanel">
                <div class="p-20">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">History</h5>
                            <div class="form-group row">

                                <label class="col-md-3 m-t-15">Select Status Type</label>
                                <div class="col-md-9">
                                    <asp:DropDownList ID="ddlTimeSheetStatus" class="select2 form-control custom-select" Style="width: 100%; height: 36px;" runat="server" OnSelectedIndexChanged="ddlTimeSheetStatus_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>


                                <label class="col-md-3 m-t-15">Select Project</label>
                                <div class="col-md-9">
                                    <asp:DropDownList ID="ddlProject" class="select2 form-control custom-select" Style="width: 100%; height: 36px;" runat="server" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>


                                <label class="col-md-3 m-t-15">Select Month</label>
                                <div class="col-md-9">
                                    <asp:DropDownList ID="ddlMonth" class="select2 form-control custom-select" Style="width: 100%; height: 36px;" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:Button ID="btnGetReport" runat="server" Visible="false" Text="Get Report" OnClick="getReport_Click" />
                                </div>

                            </div>


                            <%-- <asp:UpdatePanel ID="updHist" runat="server" UpdateMode="Always">
                                <ContentTemplate>--%>
                            <asp:GridView ID="gvTimeSheet" class="table table-striped" Width="100%" OnRowCommand="gvTimeSheet_RowCommand" OnRowDataBound="gvTimeSheet_RowDataBound" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                <Columns>
                                    <%--<asp:BoundField DataField="First_Name" HeaderText="Name" />--%>
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Day" HeaderText="Day" Visible="false" />
                                    <asp:BoundField DataField="Project" HeaderText="Project" />
                                    <asp:BoundField DataField="InOut" HeaderText="In/Out Time" />
                                    <asp:BoundField DataField="Hours" HeaderText="Hours" />
                                    <asp:BoundField DataField="Desc" HeaderText="Description" Visible="false" />
                                    <asp:BoundField DataField="OT" HeaderText="O.T" />
                                    <asp:BoundField DataField="reprt" HeaderText="Manager" />
                                    <asp:BoundField DataField="resMgn" HeaderText="Client Manager" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnView" runat="server" Style="font-size: 25px; color: #6dd7e8;" class="m-r-10 mdi mdi-eye" Text="" CommandName="ViewRow" CommandArgument='<%# Eval("qString") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EntryNo" HeaderText="Entry No" Visible="false" />
                                    <asp:BoundField DataField="qString" HeaderText="Query String" Visible="false" />
                                </Columns>
                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                <SortedDescendingHeaderStyle BackColor="#3E3277" />
                            </asp:GridView>
                            <%--</ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTimeSheetStatus" EventName="SelectedIndexChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ddlProject" EventName="SelectedIndexChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="btnView" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>--%>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
