﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TimesheetView.aspx.cs" Inherits="TimeSheet5.TimesheetView" %>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<%--<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Timesheet View</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                            <li class="breadcrumb-item">
                                <asp:LinkButton ID="lbuttonLink" runat="server" OnClick="lbuttonLink_Click"></asp:LinkButton>
                               
                            <li class="breadcrumb-item active" aria-current="page">Employee Time Sheet View</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>--%>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!DOCTYPE html>

<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png" />
    <title>Unikul TimeSheet</title>
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <div class="card">
            <div class="card-body">
                <table class="nav-justified">
                    <li class="breadcrumb-item active" aria-current="page">Employee Time Sheet View</li>
                    <%--<tr>
                        <td style="width: 230px;"></td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labNamed" runat="server"></asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="width: 230px;">Name</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Project Name</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labProjName" runat="server" Style="font-weight: 700"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Client Manager</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labRespP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Activity</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labActivity" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Day</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labDay" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Date</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Submitted Date</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labSubmittedDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">In-Out</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labInOut" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Regular Hours</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labHours" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Over Time</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labOT" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">Description</td>
                        <td style="width: 30px; text-align: center">:</td>
                        <td>
                            <asp:Label ID="labDesc" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px;">&nbsp;</td>
                        <td style="width: 30px; text-align: center">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <span style="font-weight: 700;">Approval Details </span>
                <br />

                <asp:Panel ID="Panel1" runat="server" Visible="false">
                    <asp:Label ID="labProjectTimesheetType" runat="server" Text="" Style="text-align: left"></asp:Label>

                    <br />

                    <table class="nav-justified">
                        <tr>
                            <td style="width: 230px">Status</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labIntrStatus" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Approved Date</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labInterDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Approved By</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labApprovedBy" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">&nbsp;</td>
                            <td style="width: 31px; text-align: center">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" Visible="false">
                    Escalation Level 2 Project<br />
                    &nbsp;<table class="nav-justified">
                        <tr>
                            <td style="width: 230px">Status</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv2Staus" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Client Manager</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv2CM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Client Manager Approved Date</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv2CMappproveDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Report Manager</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv2RM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Report Manager Approved Date</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="lablv2RMapprovedDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">&nbsp;</td>
                            <td style="width: 31px; text-align: center">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Panel3" runat="server" Visible="false">
                    Escalation Level 3 Project<br />
                    &nbsp;<table class="nav-justified">
                        <tr>
                            <td style="width: 230px">Status</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3Status" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Report Manager</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3RM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Report Manager Approved Date 1</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3RMapprovedDate1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Client Manager</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3CM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Client Manager Approved Date</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3CMapprovedDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Report Manager Approved Date 2</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labLv3RMapprovedDate2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">&nbsp;</td>
                            <td style="width: 31px; text-align: center">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>

                <asp:Panel ID="Panel4" runat="server" Visible="false">
                    Reject Details<br />
                    &nbsp;<table class="nav-justified">
                        <tr>
                            <td style="width: 230px">Status</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labRejectStatus" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Manager</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labRejectManager" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 230px">Reject Date</td>
                            <td style="width: 31px; text-align: center">:</td>
                            <td>
                                <asp:Label ID="labRejectDate" runat="server"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 230px">&nbsp;</td>
                            <td style="width: 31px; text-align: center">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>


            </div>
        </div>
    </form>
</body>
</html>

