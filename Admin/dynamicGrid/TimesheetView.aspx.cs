﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TimeSheet5.JobCardWS;
using TimeSheet5.TimesheetEntryWS;
using TimeSheet5.ResourceCardWS;
using TimeSheet5.EmployeeCardWS;


namespace TimeSheet5
{
    public partial class TimesheetView : System.Web.UI.Page
    {
        string entrNo = "";
        string FName = "";
        string LName = "";
        string resourceName = "";
        string ProjectName = "";
        string ApprovalStatus = "";
        string view = "";
        string rejSts = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                entrNo = Request.QueryString["entryNo"];
                FName = Request.QueryString["FName"];
                LName = Request.QueryString["LName"];
                resourceName = Request.QueryString["eCode"];
                ProjectName = Request.QueryString["ProjName"];
                ApprovalStatus = Request.QueryString["sts"];
                view = Request.QueryString["v"];
                rejSts = Request.QueryString["rej"];

                //if (view == "1")
                //    lbuttonLink.Text = "Timesheet History";
                //else
                //    lbuttonLink.Text = "Employee Timesheet";

                getTimesheetDet();
            }

        }

        private void getTimesheetDet()
        {
            EmployeeCard_Service em_service = new EmployeeCard_Service();
            em_service.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            ResourceCard_Service resource_service = new ResourceCard_Service();
            resource_service.Credentials = new NetworkCredential("administrator", "U@ss##156#");


            TimesheetEntry_Service empTimeServ = new TimesheetEntry_Service();
            empTimeServ.Credentials = new NetworkCredential("administrator", "U@ss##156#");

            TimesheetEntry t = empTimeServ.Read(Int32.Parse(entrNo));
             ResourceCard r=new ResourceCard();
            if(t.Responsible_resource!=null)
                r = resource_service.Read(t.Responsible_resource);
            
            EmployeeCard em = em_service.Read(t.Reporting_To);



            labName.Text = FName + " " + LName;
            labProjName.Text = t.Project_Name;
            labRespP.Text = r.Name;
            labActivity.Text = t.Job_Task_Line;
            labDay.Text = t.Day;
            labDate.Text = t.Date.ToShortDateString();
            labSubmittedDate.Text = t.Submit_Date.ToShortDateString();
            labInOut.Text = t.In_time.ToShortTimeString() + " - " + t.Out_time.ToShortTimeString();
            labHours.Text = t.Regular_Hours.ToString();
            labOT.Text = t.OverTime.ToString();
            labDesc.Text = t.Task_Description;


            string d1 = "";
            string d2 = "";
            string d3 = "";
            if (t.Escalation_Level == 0)
            {
                Panel1.Visible = true;
                labProjectTimesheetType.Text = "Parallel Project";
                labIntrStatus.Text = ApprovalStatus;
                if (t.Approved_by_CM)
                {
                    labApprovedBy.Text = "Approval Cilent Manager " + r.Name;
                    labInterDate.Text = t.CM_Approved_date.ToShortDateString();
                }
                else if (t.Approved_by_reporting_manager)
                {
                    labApprovedBy.Text = "Approval Internal Manager " + em.First_Name + " " + em.Last_Name;
                    labInterDate.Text = t.RM_Approved_date.ToShortDateString();
                }
                else if (t.Approved_by_reporting_manager && t.Approved_by_CM)
                {
                    labApprovedBy.Text = "Approval Internal Manager " + em.First_Name + " " + em.Last_Name + " and Approval Cilent Manager " + r.Name;
                    labInterDate.Text = t.RM_Approved_date.ToShortDateString();
                }

            }
            else if (t.Escalation_Level == 1)
            {
                Panel1.Visible = true;
                labProjectTimesheetType.Text = "Internal Project";
                labIntrStatus.Text = ApprovalStatus;
                labApprovedBy.Text = "Approval Internal Manager " + em.First_Name + " " + em.Last_Name;
                labInterDate.Text = t.RM_Approved_date.ToShortDateString();
            }
            else if (t.Escalation_Level == 2)
            {

                Panel2.Visible = true;
                labLv2Staus.Text = ApprovalStatus;
                labLv2CM.Text = r.Name;
                if (t.CM_Approved_date == Convert.ToDateTime("01-01-0001"))
                    d1 = "Pending";
                else
                    d1 = Convert.ToDateTime(t.CM_Approved_date).ToShortDateString();
                labLv2CMappproveDate.Text = d1;
                labLv2RM.Text = em.First_Name + " " + em.Last_Name;
                if (t.RM_Approved_date == Convert.ToDateTime("01-01-0001"))
                    d2 = "Pending";
                else
                    d2 = Convert.ToDateTime(t.RM_Approved_date).ToShortDateString();
                lablv2RMapprovedDate.Text = d2;
            }
            else if (t.Escalation_Level == 3)
            {
                Panel3.Visible = true;
                labLv3Status.Text = ApprovalStatus;
                labLv3RM.Text = em.First_Name + " " + em.Last_Name;
                if (t.RM_Approved_date == Convert.ToDateTime("01-01-0001"))
                    d1 = "Pending";
                else
                    d1 = Convert.ToDateTime(t.RM_Approved_date).ToShortDateString();
                labLv3RMapprovedDate1.Text = d1;
                labLv3CM.Text = r.Name;
                if (t.CM_Approved_date == Convert.ToDateTime("01-01-0001"))
                    d2 = "Pending";
                else
                    d2 = Convert.ToDateTime(t.CM_Approved_date).ToShortDateString();
                labLv3CMapprovedDate.Text = d2;
                if (t.RM_Approved_Date2 == Convert.ToDateTime("01-01-0001"))
                    d3 = "Pending";
                else
                    d3 = Convert.ToDateTime(t.RM_Approved_Date2).ToShortDateString();
                labLv3RMapprovedDate2.Text = d3;
            }
            
            if(t.Status==TimeSheet5.TimesheetEntryWS.Status.Rejected)
            {
                Panel4.Visible = true;
                labRejectDate.Text = t.RejectDate.ToShortDateString();
                labRejectManager.Text = t.RejectBy;
                labRejectStatus.Text = rejSts;
            }

        }

        protected void lbuttonLink_Click(object sender, EventArgs e)
        {
            //if (lbuttonLink.Text ==  "Timesheet History")
            //    Response.Redirect("/TimeSheet.aspx");
            //else
            //    Response.Redirect("/EmpTimeSheetApprove.aspx");

        }
    }
}