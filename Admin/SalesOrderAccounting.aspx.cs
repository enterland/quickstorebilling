﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickStoreBilling.Reports;
using System.Data;
using System.Reflection;
using System.Text.RegularExpressions;

namespace QuickStoreBilling.Admin
{
    public partial class SalesOrderAccounting : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public int CustomerIDView = 0;
        public string fromDate = null;
        public string toDate = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "ad")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }

            if (!IsPostBack)
            {
                getOrderAccountingGrid();
                fillDdlddlCustomerView();
            }
        }
        public void fillDdlddlCustomerView()
        {
            ddlCustomerView.Items.Clear();
            var cat = db.users.Where(l => l.userCatId == 2).Select(m => new { id = m.uId, name = m.name }).ToList();


            ddlCustomerView.DataSource = cat;
            ddlCustomerView.DataTextField = "name";
            ddlCustomerView.DataValueField = "id";
            ddlCustomerView.DataBind();
            ddlCustomerView.Items.Insert(0, "Select");
        }

        private void selectedViewCustomer() // gridview selcted viewcategory  
        {
            if (ddlCustomerView.SelectedIndex == 0)
                CustomerIDView = 0;
            else
                CustomerIDView = Int32.Parse(ddlCustomerView.SelectedItem.Value);
            Session["c"] = CustomerIDView;

            txtFromDate.Text = "";
            fromDate = null;
            Session["from"] = "";
            txtToDate.Text = "";
            toDate = null;
            Session["to"] = "";
            txtSearch.Text = "";
        }

        private void textChangeViewDate() // gridview Date view text change
        {
            ddlCustomerView.SelectedIndex = 0;
            Session["c"] = CustomerIDView;
            if (txtFromDate.Text != "")
            {
                fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy");
                Session["from"] =fromDate;
            }
                
            else
                return;
            if (txtToDate.Text != "")
            {
                toDate = Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy");
                Session["to"] = toDate;
            }
                
            else
                return;
            
            txtSearch.Text = "";
        }

        private void textChangeViewSearch() // gridview Date view serach text
        {
            ddlCustomerView.SelectedIndex = -1;
            Session["c"] = CustomerIDView;
            txtFromDate.Text = "";
            fromDate = null;
            Session["from"] = "";
            txtToDate.Text = "";
            toDate = null;
            Session["to"] = "";
        }

        private void getOrderAccountingGrid()
        {
            AccountingDSClass tr = new AccountingDSClass();
            DataSet ds = new DataSet();


            ds = tr.getAccount(CustomerIDView, fromDate, toDate);

            gvBillingList.DataSource = ds;
            gvBillingList.DataBind();
        }

        private void filterGridSync() // for editiing, deleting and cancel selected filter syncronisiatin with grid
        {
            if (fromDate != null && toDate != null)
            {
                textChangeViewDate();
                getOrderAccountingGrid();
            }
            else if (ddlCustomerView.SelectedIndex > 0)
            {
                selectedViewCustomer();
                getOrderAccountingGrid();
            }
            else if ( fromDate==null && toDate==null && ddlCustomerView.SelectedIndex == 0 && txtSearch.Text == "")
            {
                getOrderAccountingGrid();
            }
            else
            {
                this.SearchText();
            }
        }
        protected void gvBillingList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBillingList.PageIndex = e.NewPageIndex;
            filterGridSync();
        }

        /// dynamic serach////
        /// 

        private DataTable GetRecords()

        {
            AccountingDSClass tr = new AccountingDSClass();
            DataSet ds = new DataSet();

            ds = tr.getAccount(CustomerIDView, fromDate, toDate);

            gvBillingList.DataSource = ds;
            gvBillingList.DataBind();

            DataTable dt = ds.Tables["Table1"];

            return dt;
        }

        private void SearchText()
        {
            DataTable dt = GetRecords();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvBillingList.SortExpression, txtSearch.Text);
            }

            try
            {
                if (SearchExpression != null)
                {
                    dv.RowFilter = "CustomerName like" + SearchExpression;
                    gvBillingList.DataSource = dv;
                    gvBillingList.DataBind();
                }
            }
            catch (Exception Ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + Ex.Message + "')</SCRIPT>");
            }
        }

        public string Highlight(string InputTxt)
        {
            string Search_Str = txtSearch.Text.ToString();
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(),
            RegexOptions.IgnoreCase);

            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt,
            new MatchEvaluator(ReplaceKeyWords));

            // Set the RegExp to null.
            RegExp = null;
        }

        public string ReplaceKeyWords(Match m)
        {
            return "<span class='highlight'>" + m.Value + "</span>";

        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            textChangeViewSearch();
            SearchText();
        }

        protected void LinkButtonBillNUmber_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvRow = (GridViewRow)lb.NamingContainer;
            int rowID = gvRow.RowIndex;

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "openModal", "window.open('/BillingCard.aspx?o=" + lb.Text + "' ,'_blank');", true);
        }

        protected void ddlCustomerView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedViewCustomer();
            getOrderAccountingGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            textChangeViewDate();
            if (fromDate != "" && toDate != "")
            {
                getOrderAccountingGrid();
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string c = "";
            string f = "";
            string t = "";
            if (txtSearch.Text != "")
                return;
            else
            {
                if (Session["c"] == null)
                    c = "";
                else
                    c = Session["c"].ToString();
                if (Session["from"] == null)
                    f = "";
                else
                    f = Session["from"].ToString();
                if (Session["to"] == null)
                    t = "";
                else
                    t = Session["to"].ToString();

                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(),
                    "openModal", "window.open('/Admin/SOAccountingPrint.aspx?c=" + c+ "&from=" + f + "&to=" + t+"' ,'_blank');", true);
            }
        }
    }
}