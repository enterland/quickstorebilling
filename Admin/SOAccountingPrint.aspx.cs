﻿using Microsoft.Reporting.WebForms;
using QuickStoreBilling.Model;
using QuickStoreBilling.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class SOAccountingPrint : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public string from, to;
        int cid=0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() != "ad")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }


            if (!IsPostBack)
            {
                if(Request.QueryString["c"]!="")
                    cid = Convert.ToInt32(Request.QueryString["c"].ToString());
                
                if (Request.QueryString["from"]!="")
                    from = Request.QueryString["from"].ToString();

                if(Request.QueryString["to"]!="")
                    to = Request.QueryString["to"].ToString();

                getSOAccountingReport(cid,from,to);
            }


        }
        public void getSOAccountingReport(int customerId, string fromDate, string toDate)
        {
            try
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/AccountingReport.rdlc");

                AccountingDSClass tr = new AccountingDSClass();
                DataSet ds = new DataSet();

                ds = tr.getAccount(customerId, fromDate, toDate);

                ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("AccountingDataSet", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}