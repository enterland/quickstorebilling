﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Ind.aspx.cs" Inherits="QuickStoreBilling.Admin.Ind" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ProdId" DataSourceID="SqlDataSourceProd">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ProdId" HeaderText="ProdId" ReadOnly="True" SortExpression="ProdId" Visible="False" />
                <asp:TemplateField HeaderText="Category" SortExpression="CatId">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceCat" DataTextField="name" DataValueField="CatId" SelectedValue='<%# Bind("CatId") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSourceCat" DataTextField="name" DataValueField="CatId" SelectedValue='<%# Bind("CatId") %>'>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type" SortExpression="TypeId">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSourceType" DataTextField="name" DataValueField="ProductTypeId" SelectedValue='<%# Bind("TypeId") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSourceType" DataTextField="name" DataValueField="ProductTypeId" SelectedValue='<%# Bind("TypeId") %>'>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
                <asp:BoundField DataField="description" HeaderText="Description" SortExpression="description" />
                <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" />
                <asp:BoundField DataField="qty" HeaderText="StokQty" SortExpression="qty" />
                <asp:BoundField DataField="size" HeaderText="size" SortExpression="size" Visible="False" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSourceProd" runat="server" ConnectionString="<%$ ConnectionStrings:QuickStoreDBConnectionString %>" DeleteCommand="DELETE FROM [Product] WHERE [ProdId] = @ProdId" InsertCommand="INSERT INTO [Product] ([name], [size], [description], [price], [CatId], [TypeId], [qty]) VALUES (@name, @size, @description, @price, @CatId, @TypeId, @qty)" SelectCommand="SELECT * FROM [Product]" UpdateCommand="UPDATE [Product] SET [name] = @name, [size] = @size, [description] = @description, [price] = @price, [CatId] = @CatId, [TypeId] = @TypeId, [qty] = @qty WHERE [ProdId] = @ProdId">
            <DeleteParameters>
                <asp:Parameter Name="ProdId" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="size" Type="String" />
                <asp:Parameter Name="description" Type="String" />
                <asp:Parameter Name="price" Type="Double" />
                <asp:Parameter Name="CatId" Type="Int32" />
                <asp:Parameter Name="TypeId" Type="Int32" />
                <asp:Parameter Name="qty" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="size" Type="String" />
                <asp:Parameter Name="description" Type="String" />
                <asp:Parameter Name="price" Type="Double" />
                <asp:Parameter Name="CatId" Type="Int32" />
                <asp:Parameter Name="TypeId" Type="Int32" />
                <asp:Parameter Name="qty" Type="Int32" />
                <asp:Parameter Name="ProdId" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSourceCat" runat="server" ConnectionString="<%$ ConnectionStrings:QuickStoreDBConnectionString %>" SelectCommand="SELECT * FROM [ProductCategory]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSourceType" runat="server" ConnectionString="<%$ ConnectionStrings:QuickStoreDBConnectionString %>" SelectCommand="SELECT [ProductTypeId], [name] FROM [ProductType]"></asp:SqlDataSource>
    </div>
    <div>
        <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    
                        <ContentTemplate>
                            <asp:GridView ID="gvProducts" runat="server" DataKeyNames="id" OnRowDataBound="gvProducts_RowDataBound"
                                OnRowEditing="gvProducts_RowEditing" OnRowCancelingEdit="gvProducts_RowCancelingEdit"
                                OnRowUpdating="gvProducts_RowUpdating" OnRowDeleting="gvProducts_RowDeleting"
                                EmptyDataText="No records has been added." AutoGenerateEditButton="True" AutoGenerateDeleteButton="True" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                    <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                                    <asp:BoundField DataField="Description" HeaderText="Description" />
                                    <asp:BoundField DataField="Price" HeaderText="Price" />
                                    <asp:BoundField DataField="Qty" HeaderText="Qty" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    
    </div>
</asp:Content>
