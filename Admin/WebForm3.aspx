﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="QuickStoreBilling.Admin.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="card-body">
        <asp:ScriptManager ID="ScriptTimeSheet" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <div class="form-group row">
            <label class="col-md-3 m-t-15">Product Entry</label>
            <div class="table-responsive">
                <%--<asp:UpdatePanel ID="updTimeSheet" UpdateMode="Always" runat="server">
                    <ContentTemplate>--%>
                        <asp:Panel ID="pnl_warning" runat="server" Visible="false">
                            <div class="alert alert-warning" role="alert" style="width: 100%">
                                <asp:Label ID="labWarning" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnl_err" runat="server" Visible="false">
                            <div class="alert alert-danger" role="alert" style="width: 100%">
                                <asp:Label ID="LabErr" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:GridView ID="gvProducts" runat="server" ShowFooter="true" CssClass="table-striped table-bordered table"
                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowHeader="true" OnRowCreated="gvProducts_RowCreated">
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="SL" />
                                <asp:TemplateField HeaderText="Category">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlCategory" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true"
                                            AppendDataBoundItems="true">
                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="recDdlCategory" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required"
                                            ControlToValidate="ddlCategory" runat="server"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    <FooterStyle />
                                    <FooterTemplate>
                                        <asp:LinkButton ID="lbAddNewRow" runat="server" Style="padding: 0;" OnClick="lbAddNewRow_Click" CssClass="m-icon"><i style="font-size: 30px;color: #2eb2da;margin-right: 0;" class="m-r-10 mdi mdi-plus-circle"></i></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlType" runat="server"
                                            AppendDataBoundItems="true" required="true">
                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="recDdlActi" InitialValue="-1" Display="Dynamic" ValidationGroup="g1" Text="*" ErrorMessage="Required"
                                            ControlToValidate="ddlType" runat="server"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtQty" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbDel" runat="server" Style="padding: 0;" OnClick="lbDel_Click"><i style="font-size: 30px;color: #da542e;margin-right: 0;" ></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    <%--</ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvProducts" EventName="RowCreated" />
                        <asp:AsyncPostBackTrigger ControlID="ddlCategory" EventName="OnSelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>
        </div>

        <br />
        <div class="border-top">
            <div class="card-body" style="text-align: right;">
                <asp:Button class="btn btn-primary" ID="btnSubmitTimeSheet" runat="server" Text="Submit" />
            </div>
        </div>

    </div>

</asp:Content>
