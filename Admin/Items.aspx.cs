﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickStoreBilling.Model;
using EntityFramework.Extensions;
using System.Reflection;

namespace QuickStoreBilling.Admin
{
    public partial class Items : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProductGrid();

            }
        }

        private void getProductGrid()
        {


            var s = db.Products.Select(m => new
            {
                id = m.ProdId,
                CategoryName = m.ProductCategory.name,
                TypeName = m.ProductType.name,
                ProductName = m.name,
                Description = m.description,
                Price = m.price,
                Qty = m.qty
            })
                .OrderBy(k => k.ProductName).ToList();


            DataTable dt = LINQResultToDataTable(s);

            gvItems.DataSource = dt;
            gvItems.DataBind();

            SetInitialRow(dt);

        }

        public DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {
            DataTable dt = new DataTable();
            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type colType = GetProperty.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                               == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, colType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo pinfo in columns)
                {
                    dr[pinfo.Name] = pinfo.GetValue(Record, null) == null ? DBNull.Value : pinfo.GetValue
                           (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }

        private void FillDropDownListCategory(DropDownList ddlCat)                           
        {

            ddlCat.Items.Clear();
            var cat = db.ProductCategories.Select(l => new { catId = l.CatId, name = l.name }).ToList();



            ddlCat.DataSource = cat;
            ddlCat.DataTextField = "name";
            ddlCat.DataValueField = "catId";
            ddlCat.DataBind();
            ddlCat.Items.Insert(0, "Select");

        }


        private void FillDropDownListType(DropDownList ddlType)                           
        {

            ddlType.Items.Clear();
            var cat = db.ProductTypes.Select(l => new { typeId = l.ProductTypeId, name = l.name }).ToList();



            ddlType.DataSource = cat;
            ddlType.DataTextField = "name";
            ddlType.DataValueField = "typeId";
            ddlType.DataBind();
            ddlType.Items.Insert(0, "Select");

        }
        private void SetInitialRow(DataTable dt)
        {

            //DataTable dt = new DataTable();
            //DataRow dr = null;

            //dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            //dt.Columns.Add(new DataColumn("Date", typeof(string)));
            //dt.Columns.Add(new DataColumn("Project", typeof(string)));
            //dt.Columns.Add(new DataColumn("Activity", typeof(string)));
            //dt.Columns.Add(new DataColumn("In Time", typeof(string)));
            //dt.Columns.Add(new DataColumn("Out Time", typeof(string)));
            //dt.Columns.Add(new DataColumn("Hours", typeof(string)));
            //dt.Columns.Add(new DataColumn("Description", typeof(string)));


            //dr = dt.NewRow();
            //dr["RowNumber"] = 1;
            //dr["Date"] = string.Empty;
            //dr["In Time"] = string.Empty;
            //dr["Out Time"] = string.Empty;
            //dr["Hours"] = string.Empty;
            //dr["Description"] = string.Empty;

            //dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference   
            ViewState["CurrentTable"] = dt;

            //Bind the Gridview   
            gvItems.DataSource = dt;
            gvItems.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data
            //
           // Label ddlCat1 = (Label)gvItems.Rows[0].Cells[1].FindControl("Label2");
            DropDownList ddlCat = (DropDownList)gvItems.Rows[0].Cells[2].Controls[1].FindControl("ddlCategory");

            //FillDropDownListCategory(ddlCat);

            string i = "sdjf";



        }


        public void AddNewRowToGrid()
        {

            try
            {
                if (pnl_err.Visible != true)
                {
                    if (ViewState["CurrentTable"] != null)
                    {

                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;

                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            drCurrentRow = dtCurrentTable.NewRow();
                            //drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                            //add new row to DataTable   
                            dtCurrentTable.Rows.Add(drCurrentRow);
                            //Store the current data to ViewState for future reference   

                            ViewState["CurrentTable"] = dtCurrentTable;


                            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                            {

                                //extract the TextBox values   

                                TextBox box1 = (TextBox)gvItems.Rows[i].Cells[4].FindControl("txtName");
                                TextBox box2 = (TextBox)gvItems.Rows[i].Cells[5].FindControl("txtDescription");
                                TextBox box3 = (TextBox)gvItems.Rows[i].Cells[6].FindControl("txtPrice");
                                TextBox box4 = (TextBox)gvItems.Rows[i].Cells[6].FindControl("txtQty");
                                

                                dtCurrentTable.Rows[i]["ProductName"] = box1.Text;
                                dtCurrentTable.Rows[i]["Description"] = box2.Text;
                                dtCurrentTable.Rows[i]["Price"] = box3.Text;
                                dtCurrentTable.Rows[i]["Qty"] = box4.Text;
                               

                                //extract the DropDownList Selected Items   

                                DropDownList ddlCategory = (DropDownList)gvItems.Rows[i].Cells[2].FindControl("ddlCategory");

                                // Update the DataRow with the DDL Selected Items   

                                dtCurrentTable.Rows[i]["Category"] = ddlCategory.SelectedItem.Text;

                                //job = ddl1.SelectedItem.Text;
                                //NonEditedFieldLeaveDay(job, box2, box3, box4);


                                DropDownList ddlType = (DropDownList)gvItems.Rows[i].Cells[3].FindControl("ddlType");
                                dtCurrentTable.Rows[i]["Type"] = ddlType.SelectedItem.Text;



                            }

                            //Rebind the Grid with the current data to reflect changes   
                            gvItems.DataSource = dtCurrentTable;
                            gvItems.DataBind();
                        }
                    }
                    else
                    {
                        Response.Write("ViewState is null");

                    }
                    //Set Previous Data on Postbacks   
                    SetPreviousData();
                }

            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }

        private void SetPreviousData()
        {

            try
            {
                int rowIndex = 0;
                if (ViewState["CurrentTable"] != null)
                {

                    DataTable dt = (DataTable)ViewState["CurrentTable"];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TextBox box1 = (TextBox)gvItems.Rows[i].Cells[1].FindControl("txtDate");
                            TextBox box2 = (TextBox)gvItems.Rows[i].Cells[4].FindControl("txtIn");
                            TextBox box3 = (TextBox)gvItems.Rows[i].Cells[5].FindControl("txtOut");
                            TextBox box4 = (TextBox)gvItems.Rows[i].Cells[6].FindControl("txtHour");
                            TextBox box5 = (TextBox)gvItems.Rows[i].Cells[7].FindControl("txtDesc");


                            DropDownList ddlCat = (DropDownList)gvItems.Rows[rowIndex].Cells[2].FindControl("ddlCategory");
                            DropDownList ddlType = (DropDownList)gvItems.Rows[rowIndex].Cells[3].FindControl("ddlType");

                            //Fill the DropDownList with Data   
                            FillDropDownListCategory(ddlCat);

                            int iu = ddlCat.Items.Count;
                            //FillDropDownList2(ddl2);

                            if (i < dt.Rows.Count - 1)
                            {

                                //Assign the value from DataTable to the TextBox   
                                box1.Text = dt.Rows[i]["Name"].ToString();
                                box2.Text = dt.Rows[i]["Description"].ToString();
                                box3.Text = dt.Rows[i]["Price"].ToString();
                                box4.Text = dt.Rows[i]["Qty"].ToString();
                               

                                //Set the Previous Selected Items on Each DropDownList  on Postbacks   
                                ddlCat.ClearSelection();
                                ddlCat.Items.FindByText(dt.Rows[i]["Category"].ToString()).Selected = true;
                                //job = ddl1.SelectedValue;
                                //NonEditedFieldLeaveDay(job, box2, box3, box4);
                                FillDropDownListCategory(ddlCat);


                                ddlType.ClearSelection();
                                int iue = ddlCat.Items.Count;
                                ddlCat.Items.FindByText(dt.Rows[i]["Type"].ToString()).Selected = true;

                                // Validation on Calculate MinHour,MaxHour, TimesheetEntry On Leave 
                                pnl_warning.Visible = true;
                                
                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!! " + ex.Message + "')</SCRIPT>");
            }
        }

        protected void gvItems_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //AddNewRowToGrid();
        }

        protected void btnAddProduct_Click(object sender, ImageClickEventArgs e)
        {
            AddNewRowToGrid();
        }
    }
}