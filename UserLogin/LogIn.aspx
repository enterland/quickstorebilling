﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="QuickStoreBilling.UserLogin.LogIn" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="/Admin/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/Admin/assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="/Admin/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="/Admin/Image/sarralleLogo.png" />
</head>
<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth">
                <div class="row flex-grow">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left p-5">
                            <div class="brand-logo" style="text-align: center;">
                                <img src="/Admin/assets/images/favicon.png">
                            </div>
                            <%--<h4>Hello! let's get started</h4>
                            <h6 class="font-weight-light">Sign in to continue.</h6>--%>
                            <form class="pt-3" runat="server">
                                <div class="form-group">
                                    <asp:TextBox ID="txtUserName" class="form-control form-control-lg" required placeholder="Username" runat="server" />
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control form-control-lg" required placeholder="Password" runat="server" />
                                </div>
                                <div class="mt-3">
                                    <%--<a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">SIGN IN</a>--%>
                                    <asp:Button ID="btnSignIn" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" runat="server" Text="SIGN IN" OnClick="btnSignIn_Click"/>
                                </div>
                               <%-- <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <asp:TextBox ID="checkbox" class="form-check-input">
                                            Keep me signed in
                                        </label>
                                    </div>
                                    <a href="#" class="auth-link text-black">Forgot password?</a>
                                </div>
                                <div class="mb-2">
                                    <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                                        <i class="mdi mdi-facebook mr-2"></i>Connect using facebook
                                    </button>
                                </div>
                                <div class="text-center mt-4 font-weight-light">
                                    Don't have an account? <a href="Register.aspx" class="text-primary">Create</a>
                                </div>--%>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="/Admin/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="/Admin/assets/js/off-canvas.js"></script>
    <script src="/Admin/assets/js/hoverable-collapse.js"></script>
    <script src="/Admin/assets/js/misc.js"></script>
    <!-- endinject -->
</body>
</html>

