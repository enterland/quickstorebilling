﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling.UserLogin
{
    public partial class LogIn : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            var log = db.users.SingleOrDefault(m => m.userName == txtUserName.Text && m.pass == txtPassword.Text);
            if (log == null)
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('!!!Invalid user or password')</SCRIPT>");
            else
            {
                if (log.name=="admin")
                {
                    Session["user"] = "ad";
                    Response.Redirect("/Admin/Products.aspx");
                }

                if(log.name == "billing")
                {
                    Session["user"] = "bl";
                    Response.Redirect("/Billing.aspx");
                }

                if (log.name == "stock")
                {
                    Session["user"] = "sa";
                    Response.Redirect("/Admin/Products.aspx");
                }

            }
        }
    }
}