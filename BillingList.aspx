﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BillingList.aspx.cs" Inherits="QuickStoreBilling.BillingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Billing List </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sales Order</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div style="margin: 10px; overflow-x: auto">
            <%--<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvProdUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
            <div class="form-group row mrg">
                <label for="exampleInputUsername2" style="text-align: right" class="col-sm-2 fev pad">Customer : </label>
                <div class="col-sm-2 fev">
                    <asp:DropDownList ID="ddlCustomerView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlCustomerView_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <label for="exampleInputUsername2" style="text-align: right" class="col-sm-3 fev pad">Bill Number: </label>
                <div class="col-sm-3 fev">
                    <asp:DropDownList ID="ddlBillView" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlBillView_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            Search Name:<asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" OnTextChanged="txtSearch_TextChanged" />
            <asp:GridView ID="gvBillingList" runat="server" DataKeyNames="id"
                EmptyDataText="No records has been added."
                AutoGenerateColumns="False" Width="100%" ShowFooter="True"
                CellPadding="3"
                OnPageIndexChanging="gvBillingList_PageIndexChanging" PageSize="25" AllowPaging="True" EnableTheming="True"
                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                    <asp:BoundField DataField="BillDate" HeaderText="Bill Date" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:TemplateField HeaderText="Bill Number" HeaderStyle-CssClass="GridHeader">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButtonBillNUmber" runat="server" Text='<%# Bind("BillNumber") %>' OnClick="LinkButtonBillNUmber_Click"></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />
                    <asp:BoundField DataField="OrderSingleUnitQty" HeaderText="Single<br/>Qty" HtmlEncode="false" HeaderStyle-CssClass="GridHeader" Visible="false">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Contact" HeaderText="Contact" />
                    <asp:BoundField DataField="TotalBillAmount" HeaderText="Bill<br/>Amount" HtmlEncode="false" HeaderStyle-CssClass="GridHeader" DataFormatString="{0:n}">
                        <HeaderStyle CssClass="GridHeader"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>

                </Columns>
                <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerSettings PreviousPageText="Previous" NextPageText="Next" FirstPageText="First" LastPageText="Last" PageButtonCount="10" />
                <PagerStyle CssClass="Pager" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <%--</ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="gvProducts" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
        </div>
    </div>
</asp:Content>
