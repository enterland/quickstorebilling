﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace QuickStoreBilling.Reports
{
    public class AccountingDSClass : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public DataSet getAccount(int customerId, string fromDate, string toDate)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("OrderDate", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("OrderId", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("CustomerName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Contact", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Address", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("DiscountAmount", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("GstAmount", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ProfitAmount", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("BillTotal", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("IsCancelled", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromAddress", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromMobile", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromGSTIN", Type.GetType("System.String")));

            if (customerId > 0 && fromDate == null && toDate == null)
            {
               var ld = db.Orders.Where(l => l.uid == customerId).OrderBy(k=>k.billDate);
                
                var FromAddress = db.addresses.SingleOrDefault(s => s.uId == 6);
                string frndAddrss = FromAddress.adrLine1 + ", " + FromAddress.adrLine2 + ", " + FromAddress.districtCity + ", "
                    + FromAddress.state + ", " + FromAddress.pin;
                string frndPhone = FromAddress.MobileNo;
                string gstin = FromAddress.user.GSTIN;
                string name = FromAddress.FullName;


                foreach (Order l in ld)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = Convert.ToDateTime(l.billDate).ToShortDateString();
                    dr[1] = l.orderId;
                    dr[2] = l.user.name;
                    dr[3] = l.user.phone;
                    dr[4] = l.address.adrLine1;
                    dr[5] = l.billDiscount;
                    dr[6] = l.gstAmount;
                    dr[7] = l.profitMargin;
                    dr[8] = l.billTotal;
                    dr[9] = l.IsCancel;
                    dr[10] = name;
                    dr[11] = frndAddrss;
                    dr[12] = frndPhone;
                    dr[13] = gstin;

                    dt.Rows.Add(dr);
                }
            }
            else if (customerId == 0 && fromDate != null && toDate != null)
            {
                DateTime d1 = Convert.ToDateTime(fromDate);
                DateTime d2 = Convert.ToDateTime(toDate);
               var ld = db.Orders.Where(l => l.billDate>=d1 && l.billDate<=d2).OrderBy(k => k.billDate);
                
                var FromAddress = db.addresses.SingleOrDefault(s => s.uId == 6);
                string frndAddrss = FromAddress.adrLine1 + ", " + FromAddress.adrLine2 + ", " + FromAddress.districtCity + ", "
                    + FromAddress.state + ", " + FromAddress.pin;
                string frndPhone = FromAddress.MobileNo;
                string gstin = FromAddress.user.GSTIN;
                string name = FromAddress.FullName;


                foreach (Order l in ld)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = l.billDate;
                    dr[1] = l.orderId;
                    dr[2] = l.user.name;
                    dr[3] = l.user.phone;
                    dr[4] = l.address.adrLine1;
                    dr[5] = l.billDiscount;
                    dr[6] = l.gstAmount;
                    dr[7] = l.profitMargin;
                    dr[8] = l.billTotal;
                    dr[9] = l.IsCancel;
                    dr[10] = name;
                    dr[11] = frndAddrss;
                    dr[12] = frndPhone;
                    dr[13] = gstin;

                    dt.Rows.Add(dr);
                }
            }
            else
            {
                var ld = db.Orders.OrderBy(k => k.billDate);
               
                var FromAddress = db.addresses.SingleOrDefault(s => s.uId == 6);
                string frndAddrss = FromAddress.adrLine1 + ", " + FromAddress.adrLine2 + ", " + FromAddress.districtCity + ", "
                    + FromAddress.state + ", " + FromAddress.pin;
                string frndPhone = FromAddress.MobileNo;
                string gstin = FromAddress.user.GSTIN;
                string name = FromAddress.FullName;


                foreach (Order l in ld)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = l.billDate;
                    dr[1] = l.orderId;
                    dr[2] = l.user.name;
                    dr[3] = l.user.phone;
                    dr[4] = l.address.adrLine1;
                    dr[5] = l.billDiscount;
                    dr[6] = l.gstAmount;
                    dr[7] = l.profitMargin;
                    dr[8] = l.billTotal;
                    dr[9] = l.IsCancel;
                    dr[10] = name;
                    dr[11] = frndAddrss;
                    dr[12] = frndPhone;
                    dr[13] = gstin;

                    dt.Rows.Add(dr);
                }
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}