﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace QuickStoreBilling.Reports
{
    public class QuotationDSClasss : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public DataSet getQuotation()
        {
            var ld = db.Quotations;

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("ProdID", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ProdName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Qty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("UnitPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("TotalPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromAddress", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromMobile", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromGSTIN", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("mrp", Type.GetType("System.String")));

            var FromAddress = db.addresses.SingleOrDefault(s => s.uId == 6);
            string frndAddrss = FromAddress.adrLine1 + ", " + FromAddress.adrLine2 + ", " + FromAddress.districtCity + ", "
                + FromAddress.state + ", " + FromAddress.pin;
            string frndPhone = FromAddress.MobileNo;
            string gstin = FromAddress.user.GSTIN;
            string name = FromAddress.FullName;


            foreach (Quotation l in ld)
            {
                DataRow dr = dt.NewRow();

                
                dr[0] = l.prodId;
                dr[1] = l.prodName;
                dr[2] = l.Qty;
                dr[3] = l.unitPriceAll;
                dr[4] = l.totalPrice;
                dr[5] = name;
                dr[6] = frndAddrss;
                dr[7] = frndPhone;
                dr[8] = gstin;
                dr[9] = db.Products.SingleOrDefault(k=>k.ProdId==l.prodId).mrp;


                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}