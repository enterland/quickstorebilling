﻿using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace QuickStoreBilling.Reports
{
    public class InvoiceDSClasss : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        public DataSet getInvoice(int uid, string orderId)
        {
            var ld = db.OrderLines.Where(s => s.uId == uid && s.orderId == orderId);

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("OrderID", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("OrderDate", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Discount", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Total", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ProdID", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ProdName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Qty", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("UnitPriceAll", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("TotalPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("GSTAmount", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("CustomerName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ContactNo", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Address", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("GSTIN", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromAddress", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromMobile", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FromGSTIN", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("UnitPrice", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Gst", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("mrp", Type.GetType("System.String")));


            var FromAddress = db.addresses.SingleOrDefault(s => s.uId == 6);
            string frndAddrss = FromAddress.adrLine1 + ", " + FromAddress.adrLine2 + ", " + FromAddress.districtCity + ", "
                + FromAddress.state + ", " + FromAddress.pin;
            string frndPhone = FromAddress.MobileNo;
            string gstin = FromAddress.user.GSTIN;
            string name = FromAddress.FullName;

            foreach (OrderLine l in ld)
            {
                DataRow dr = dt.NewRow();

                dr[0] = l.orderId;
                Order order = db.Orders.SingleOrDefault(f => f.orderId == l.orderId);
                dr[1] = Convert.ToDateTime(order.billDate).ToShortDateString();
                dr[2] = order.billDiscount;
                dr[3] = order.billTotal;
                dr[4] = l.prodId;
                dr[5] = l.prodName;
                dr[6] = l.Qty;
                dr[7] = l.unitPriceAll;
                dr[8] = l.totalPrice;
                dr[9] = order.gstAmount;
                dr[10] = l.user.name;
                dr[11] = l.user.phone;
                dr[12] = order.address.adrLine1 + ", " + order.address.adrLine2 + ", " + order.address.districtCity + ", " + order.address.state + ", " +
                    order.address.pin;
                dr[13] = l.user.GSTIN;
                dr[14] = name;
                dr[15] = frndAddrss;
                dr[16] = frndPhone;
                dr[17] = gstin;
                dr[18] = l.unitPrice;
                dr[19] = l.gst;
                dr[20] = l.Product.mrp;

                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}