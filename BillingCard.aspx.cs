﻿using EntityFramework.Extensions;
using QuickStoreBilling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickStoreBilling
{
    public partial class BillingCard : System.Web.UI.Page
    {
        QuickStoreDBEntities db = new QuickStoreDBEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }
            if (Session["user"].ToString() == "sa")
            {
                Response.Redirect("/UserLogin/Login.aspx");
            }


            if (!IsPostBack)
            {
                string billNo = Request.QueryString["o"].ToString();
                
                getOrderCustomerHeader(billNo);
                if(db.Orders.SingleOrDefault(l=>l.orderId==billNo).IsCancel==true)
                {
                    pnlOrderLineAndHeader.Visible = false;
                    pnlOrderCancelled.Visible = true;
                    return;
                }
                getOrderLine(billNo);
                getOrderFooter(billNo);
            }
        }

        

        public void getOrderLine(string billNo)
        {
            
            var s = db.OrderLines.Where(l => l.orderId == billNo).Select(m => new
            {
                id = m.orderLineId,
                ProductName = m.prodName,
                Qty = m.Qty,
                gst = m.gst,
                totalQty = (db.Products.FirstOrDefault(l => l.ProdId == m.prodId).csQty) * (db.Products.FirstOrDefault(l => l.ProdId == m.prodId).csUnitQty) + (db.Products.FirstOrDefault(l => l.ProdId == m.prodId).unitQty),
                unitPrice = m.unitPrice,
                UnitPriceAll = m.unitPriceAll,
                TotalPrice = m.totalPrice
            })
                .OrderBy(k => k.ProductName).ToList();

            gvOrder.DataSource = s;
            gvOrder.DataBind();
        }

        public void getOrderCustomerHeader(string billNo)
        {
            var s = db.Orders.Where(l => l.orderId == billNo).Select(m => new
            {
                BillNo = m.orderId,
                OrderDate = m.billDate,
                CustomerName = m.user.name,
                Contact = m.user.phone,
                Address = m.address.adrLine1 + ", " + m.address.adrLine2 + ", " + m.address.districtCity + ", " + m.address.state + ", " + m.address.pin

            }).ToList();

            DataListCustomerHeader.DataSource = s;
            DataListCustomerHeader.DataBind();
        }
        public void getOrderFooter(string billNo)
        {
            var s = db.Orders.Where(l => l.orderId == billNo).Select(m => new
            {
                GSTAmount = m.gstAmount,
                OrderTotal = m.billTotal,
                TotalDiscount = m.billDiscount,
                GSTIN = m.user.GSTIN

            }).ToList();

            DataListOrderFooter.DataSource = s;
            DataListOrderFooter.DataBind();
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            string billNo = Request.QueryString["o"].ToString();

            var orderline = db.OrderLines.Where(s => s.orderId == billNo).ToList();
            foreach(OrderLine l in orderline)
            {
                Product p = db.Products.SingleOrDefault(k => k.ProdId == l.prodId);
                decimal totalProductQty = 0;
                if (p.unitQty != null)
                {
                    totalProductQty = (decimal)((p.csUnitQty * p.csQty) + p.unitQty);
                }
                else
                {
                    totalProductQty = (decimal)(p.csUnitQty * p.csQty);
                }



                decimal remProductQty = (decimal)(totalProductQty + l.Qty);

                decimal CsQtyAdjust = (decimal)(remProductQty / p.csUnitQty);

                decimal unitQtyAdjust = (decimal)(p.csUnitQty * (CsQtyAdjust - Math.Floor(CsQtyAdjust)));

                p.csQty = (int)CsQtyAdjust;
                p.unitQty = (int)unitQtyAdjust;
                
            }
            db.SaveChanges();

            db.OrderLines.Where(s => s.orderId == billNo).Delete();

            Order order= db.Orders.SingleOrDefault(s => s.orderId == billNo);
            order.billTotal = null;
            order.gstAmount = null;
            order.billDiscount = null;
            order.profitMargin = null;
            order.IsCancel = true;
            db.SaveChanges();

            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + billNo + " Deleted !!!')</SCRIPT>");

            Response.Redirect("/BillingList.aspx");
        }

        protected void buttonPrintBill_Click(object sender, EventArgs e)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "openModal", "window.open('/BillPrint.aspx?o="+Request.QueryString["o"].ToString()+"' ,'_blank');", true);
        }
    }
}